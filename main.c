/*************************************/
// Headers
/*************************************/
#include "main.h"

/*************************************/
// Core
/*************************************/
#include "lib/Map.Cells.c"
#include "lib/Map.c"
#include "lib/Map.Printer.c"
#include "lib/Map.Solver.c"
#include "lib/World.Actions.c"
#include "lib/World.c"

/*************************************/
// Game
/*************************************/
#include "Maps/SimpleMaps.c"
#include "Agents/SimpleAgents.c"
#include "Agents/TFAgent.c"
#include "conf.h"

/*************************************/
// Main
/*************************************/
S_Action spe(WorldState w, Cell c, AgentState a) {
	return ACTION(MOVE, rand() % 4 + 1);
	if (!c->mrk)
		return ACTION(MARK, true);
	DirTag moves[5];
	DirTag dmoves[5];
	Nat32 movesi = 0;
	Nat32 dmovesi = 0;
	for(DirTag dt = 1; dt <= DirTag_EAST; dt++) {
		Cell d = Cell_deltaTags(c, Dir_Data[dt], CellTags_OPN);
		if ( d && !d->mrk )
			moves[movesi++] = dt;
		if ( d )
			dmoves[dmovesi++] = dt;
		//~ if (a->actn.tag == ActionTag_MOVE && dt == a->actn.data.MOVE)
			//~ return a->actn;
	}
	if (movesi) return ACTION(MOVE, moves[RNGMOD(Nat32, movesi)]);
	if ( rand() % 2 )
	return ACTION(MOVE, w->map->grps[GrpTag_SINK].soln[c - w->map->cells].dir);
	if (!movesi && !dmovesi)
		dmoves[dmovesi++] = DirTag_HERE;
	return ACTION(MOVE, movesi ? moves[RNGMOD(Nat32, movesi)] : dmoves[RNGMOD(Nat32, dmovesi)]);
}


typedef struct CVector {
	Flt64		tkns; //
	Flt64		fuel; // c->fuel / MAX AGENT FUEL
	Flt64		mrk; // c->mrk
	Flt64		pop; // c->pop / w->aliv
	Flt64		tag0;
	Flt64		tag1;
	Flt64		tag2;
	Flt64		tag3;
} S_CVector;

typedef struct PVector {
	Flt64		fuel; // g->fuel / MAX AGENT FUEL
	Flt64		dens; // w->pop / MAPSIZE
	Flt64		aliv; // w->aliv / w->pop
	S_CVector	view[9];
} S_DVector;


Nat64 proj_Score(AgentState g) {
	return g->scor.score;
}
Nat64 proj_Slctn(AgentState g) {
	return g->scor.slctn;
}
Nat64 proj_Moves(AgentState g) {
	Nat64 dx = g->scor.gtpos.x - g->scor.ltpos.x;
	Nat64 dy = g->scor.gtpos.y - g->scor.ltpos.y;
	return (dx * dy) * 1000 + (dx + dy) * 5;
}


typedef Nat64(*Projector)(void*);

void bubbleSort(Nat64 proj(void*), void** arr, Nat32 len) {
	AgentState swp;
	Nat32 n;
	do {
		n = 0;
		for(Nat32 i = 1; i < len; i++) {
			if (proj(arr[i - 1]) < proj(arr[i])) {
				swp = arr[i];
				arr[i] = arr[i - 1];
				arr[i - 1] = swp;
				n = i;
			}
		}
		len = n;
	} while (len > 1);
}


void merge_sort(Nat64 proj(void*), void** arr, void** tmp, Nat32 len) {

	if (len < 10) {
		bubbleSort(proj, arr, len);
		return;
	}

	merge_sort(proj, arr, tmp, len / 2);
	merge_sort(proj, arr + len / 2, tmp + len / 2, len - len / 2);

	Nat32 l = 0, r = len / 2, i = 0;
	for(Nat32 *j; l < len / 2 && r < len; i++) {
		j = proj(arr[l]) >= proj(arr[r]) ? &l : &r;
		tmp[i] = arr[*j];
		(*j)++;
	}

	for(Nat32 j = l == len / 2 ? r : l; i < len; i++, j++)
		tmp[i] = arr[j];

	for(Nat32 j = 0; j < len; j++)
		arr[j] = tmp[j];

}


int main(void) {
	//~ Int32 seed = 0; srand(seed); printf("Seed : %i\n", seed);
	//~ Int32 seed = 1679826344; srand(seed); printf("Seed : %i\n", seed);
	Int32 seed = time(NULL); srand(seed); printf("Seed : %i\n", seed);

	//~ Flt32 out[100];
	//~ char* dirpath = "/home/dader/Work/Keras//Agents/agent_model_2";
	//~ struct MyTF mytf;
	//~ tf_init(&mytf, dirpath, 8, (Int64[]) {1, 46});
	//~ tf_predict(&mytf, (Flt32[]) {0.45, 0.45, 0.45, 0.45, 1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,1.10,0.5,-0.66,0.33,0.75,}, out);
	//~ tf_free(&mytf);
	//~ for(int i = 0; i < 8; i++) printf("%f\n", out[i]);
	//~ return 0;
	//~ Nat32 yy = ~0;
	//~ printf("%u %f \n", yy, (Flt32) yy);
	//~ TF_Status* status = TF_NewStatus();
	//~ printf("size: %lu \n", sizeof(S_Cell));
	//~ printf("size: %lu \n", sizeof(long double));
	//~ printf("size: %lu \n", sizeof(RNGMOD(Nat64, 4)));


	/************************************/
	// Playground Initialisation
	/************************************/
	void** mem = ALLOCS(sizeof_MapState(mconf), sizeof_WorldState(wconf));
	MapState map = INIT(MapState, mem[0], mconf); map->conf->reset(map); Map_solve(map);
	WorldState w = INIT(WorldState, mem[1], wconf, map);


	Nat32 models = 100;
	/************************************/
	// Agents Initialisation
	/************************************/
	MyTFPool pool = NEW(MyTFPool, (models));
	World_addAgents(w, w->conf->size, NULL);

	void actioner(WorldState w) {
		#pragma omp parallel num_threads(10)
		{
			int tid = omp_get_thread_num();
			int ths = omp_get_num_threads();
			for(Nat32 a = tid * w->busy / ths, amax = (tid + 1) * w->busy / ths; a < amax; a++)
				if (w->pop[a].rslt != RESULT(DEAD)) {
					w->pop[a].actn = SmartShellAgent(w, PAM(w->map, w->pop[a].pos), &w->pop[a], &pool->modls[a * models / w->busy]);
					w->pop[a].rslt = RESULT(NULL);
				}
		}
	}

	for(Int32 i = 0; i < 10; i++) {

		printf("Start simulation %u \n", i);

		World_run(w, actioner, rconf);

		merge_sort((Projector) proj_Score, (void**) w->stk, (void**) w->que, w->busy);
		merge_sort((Projector) proj_Moves, (void**) w->stk + w->busy / 10, (void**) w->que, w->busy - w->busy / 10);

		for(Nat32 q = 0; q < w->busy / 5; q++)
			w->stk[q]->scor.slctn++;

		printf("END simulation %u with %u ticks \n", i, w->tick);
		map->conf->reset(map);
		World_reset(w);

		//~ throw("ok");
		//~ PRINT(MapState, w->map);

	}

	Score scores[models], *(scosort[models]), *(scotmp[models]); Nat64 tot = 0;
	for(Nat32 i = 0; i < models; i++) {
		scosort[i] = &scores[i];
		scores[i] = (Score) {.slctn = 0, .liftm = 0, .score = 0};
	}

	for(Nat32 q = 0; q < w->busy; q++) {
		tot += w->pop[q].scor.slctn;
		scores[q * models / w->busy].slctn += w->pop[q].scor.slctn;
		scores[q * models / w->busy].liftm += w->pop[q].scor.liftm;
		scores[q * models / w->busy].score += w->pop[q].scor.score;
	}


	Nat64 scoProj(Score* s) { return s->score; }
	merge_sort((Projector) scoProj, (void**) scosort, (void**) scotmp, models);

	for(Nat32 q = 0; q < models; q++)
		printf("[%2u] Model %3li => slctn: %4lu  liftm: %-5lu  score: %-6lu\n",
			q,
			scosort[q] - scores,
			scosort[q]->slctn,
			scosort[q]->liftm,
			scosort[q]->score
		);

	return 0;
}

