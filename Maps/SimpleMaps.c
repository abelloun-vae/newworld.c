
void Map_square(MapState map) {
	init_MapState(map, map->conf);
	for(Nat32 w = map->conf->dim.w, h = map->conf->dim.h, k = 0, k0 = w * h, x = 0, y = 0; k < k0; k++, x++) {
		if ( x == w ) {x = 0; y++;}
		CellTag t = (!x || !y || !(w - x - 1) || !(h - y - 1)) ? CellTag_WALL : CellTag_CELL;
		Cell_reset(map->cells + k, t, (Pos32) {x: x, y: y});
	}
}



MapState RandomMap_reset(MapState map, Nat32 fuels, Nat32 tokens, Nat32 traps) {

	Nat32 w = map->conf->dim.w;
	Nat32 h = map->conf->dim.h;

	/* WALL and CELL */
	Map_square(map);

	/* VOID */
	RECT(c, MAP(map, w/2 - 8, 1), 			MAP(map, w/2 + 8, h/2 - 8))		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 - 8, h/2 + 8),		MAP(map, w/2 + 8, h - 2))		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, 1, h/2 - 4),			MAP(map, w/2 - 16, h/2 + 4))		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 - w/8, h/2 - h/8),		MAP(map, w/2 + w/8, h/2 + h/8))		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w * 3 / 4, 1),			MAP(map, w * 3 / 4 + 11, h / 2))	Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w * 3 / 4, h / 2 - 5),		MAP(map, w - 2, h / 2))			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 - 8, h/8),			MAP(map, w/2 + 8, h/8))			Cell_set(c, CELL(CELL));
	RECT(c, MAP(map, w*7/8, h/2 - 10),		MAP(map, w*7/8, h/2))			Cell_set(c, CELL(CELL));
	RECT(c, MAP(map, w/8 - 10, h/2 - 10),		MAP(map, w/8 - 10, h/2 + 4))		Cell_set(c, CELL(CELL));
	RECT(c, MAP(map, w/8 + 20, h/2 - 10),		MAP(map, w/4, h/2 + 4))			Cell_set(c, CELL(CELL));

	Cell_set(Map_randomCell(map), CELL(STRT));
	Cell_set(Map_randomCell(map), CELL(GOAL));

	/* FUEL */
	for(Nat32 i = 0; i < fuels; i++)
		Cell_set(Map_randomCell(map), CELL(FUEL, {rate: 100000, incr: 100000}))->fuel = map->conf->CELL_FUEL_START;

	/* TRAP */
	for(Nat32 i = 0; i < traps; i++)
		Cell_set(Map_randomCell(map), CELL(TRAP, false));

	/* TOKN */
	for(Nat32 i = 0; i < fuels; i++)
		Map_randomCell(map)->tkns++;

	/* HOVR | FLAG */
	for(Nat32 i = 0; i < traps; i++)
		Cell_set(Map_randomCell(map), RNG(Boo) ? CELL(FLAG, RNG(Boo)) : CELL(HOVR));
		//~ Cell_set(Map_randomCell(map), CELL(HOVR));

	return map;

}



MapState CustomMap000_reset(MapState map) {

	Cell c;
	//~ MATRIX(m, map);
	Nat32 w = map->conf->dim.w;
	Nat32 h = map->conf->dim.h;

	/* WALL and CELL */
	Map_square(map);
	//~ Cell_set(MAP(map, w/2, h/2), CELL(SINK));
	//~ Cell_set(MAP(map, w/2, h/2), CELL(STRT));

	/* START */
	//~ for(Nat32 x = w / 8; x < w; x += w / 4)
	//~ for(Nat32 y = h / 8; y < h; y += h / 4)
		//~ Cell_set(MAP(map, x, y), CELL(STRT));
		//~ Cell_set(MAP(map, x, y), CELL(SINK));

	/* START */
	//~ for(Nat32 x = w / 4; x < w; x += w / 2)
	//~ for(Nat32 y = h / 4; y < h; y += h / 2)
		//~ Cell_set(MAP(map, x, y), CELL(STRT));
		//~ Cell_set(MAP(map, x, y), CELL(SINK));

	//~ Cell_set(MAP(map, w - w/16, h/2), CELL(STRT));

	for(Nat32 i = 0; i < 50; i++)
		Map_randomCell(map)->tkns++;

	return map;
}
















MapState CustomMap001_reset(MapState map) {

	Cell c;
	Nat32 w = map->conf->dim.w;
	Nat32 h = map->conf->dim.h;

	/* WALL and CELL */
	Map_square(map);

	/* Voids */

	RECT(c, MAP(map, w/2 - 8, 1), 			MAP(map, w/2 + 8, h/2 - 8)) 			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 - 8, h/2 + 8), 		MAP(map, w/2 + 8, h - 2)) 			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, 1, h/2 - 4), 			MAP(map, w/2 - 16, h/2 + 4)) 			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 + 16, h/2 - 4), 		MAP(map, w - 2, h/2 + 4)) 			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 - 8, h/2 - 4), 		MAP(map, w/2 + 8, h/2 + 4)) 			Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/8 - 4, h/8 - 2), 		MAP(map, w/4 + w/8 - 4, h/4 + h/8 - 2)) 	Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 + w/8 + 4, h/8 - 2), 	MAP(map, w/2 + w*3/8 + 4, h*3/8 - 2)) 		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/8 - 4, h/2 + h/8 + 2), 	MAP(map, w*3/8 - 4, h/2 + h*3/8 + 2)) 		Cell_set(c, CELL(VOID));
	RECT(c, MAP(map, w/2 + w/8 + 4, h/2 + h/8 + 2), MAP(map, w/2 + w*3/8 + 4, h/2 + h*3/8 + 2)) 	Cell_set(c, CELL(VOID));

	for(Nat32 i = 0; i < 400; i++)
		Map_randomCell(map)->fuel += RNGMOD(Nat64, 500) + 50;

	return map;

}




