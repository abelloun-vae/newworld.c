Small experimentation of a virtual environment for ai agents evolved using NEAT ( neuro evolution of augmenting topologies )

Agents have been built using Keras (python), implementation of the evolution algorithm still has to be done, but the difficulty of getting it to work in C makes it a pain in the neck.

Also, some research has been conducted highlighting the fact that fitness evaluation actually hinders appearance of interesting features, such naturally emerging symmetry among others.
