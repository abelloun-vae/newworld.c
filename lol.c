

Map() {
	MapConf		conf;
	MapSolution	sols;
	AgentState	grav;
	Cell		hots;
	CellGroup	grps[_GrpTags_];
	S_Cell		cells[];
}

Cell(Unit) {
	Boo	mrk = false;
	Pos32	pos = {x: 0, y: 0};
	Nat32	pop = 0;
	Cell	prv = null;
	Cell	nxt = null;
	Cell	grp = null;
	Agent	ags = null;
	Nat32	tkns = 0;
	Nat64	fuel = 0;
	Nat64	gold = 0;
}

Void:Cell(null) {}
Wall:Cell(null) {}
Cell:Cell(null) {}

Fuel:Cell() {
	Nat64	rate;
	Nat64	incr;

	void step(Cell this, MapState map, Nat64 tick) {
		this->fuel += MIN();
	}
}

Gold:Cell {
	Nat64	rate;
	Nat64	incr;
}



