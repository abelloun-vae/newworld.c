
RunConf rconf = &(S_RunConf){
	.WAIT			= true,
	.ANIMATE		= 0,
	.TIMEOUT		= 1000,
	.SHOW_START		= true,
	.SHOW_END		= false,
};

MapConf mconf = &(S_MapConf) {
	//~ .reset			= &CustomMap000_reset,
	.reset			= &CustomMap001_reset,
	//~ .reset			= &RandomMap_reset,
	.CELL_FUEL_START	= 100000000,
	.CELL_FUEL_MAX		= 100000000,
	.CELL_GOLD_START	= 100000000,
	.CELL_GOLD_MAX		= 1000000000,
	.TRAP_FUEL_PENALTY	= 9,
	.TRAP_GOLD_PENALTY	= 0,
	//~ .RESPAWN		= CellTags_STRT,
	.RESPAWN		= CellTags_CELL,
	//~ .RESPAWN		= 0,
	//~ .dim.w = 319, .dim.h = 95,	// eterm zoom-2
	//~ .dim.w = 274, .dim.h = 88,	// eterm zoom-1
	//~ .dim.w = 951, .dim.h = 292,	// kterm zoom-2
	//~ .dim.w = 475, .dim.h = 128,	// kterm zoom-1
	.dim.w = 271, .dim.h = 81,	// kterm zoom-1
	//~ .dim.w = 5000, .dim.h = 1000,	// kterm zoom-1
};

WorldConf wconf = &(S_WorldConf) {
	.size			= 1000,
	.epchs			= 100000,
	.AGENT_FUEL_START	= 400,
	.AGENT_FUEL_MAX		= 1200,
	.AGENT_GOLD_START	= 0,
	.AGENT_GOLD_MAX		= 100,
	.AGENT_TOKN_START	= false,
	.COSTS			= {
	/*ACTION(WAIT)*/	{.fuel=  1, .gold= 0},
	/*ACTION(MARK)*/	{.fuel=  5, .gold= 0},
	/*ACTION(MOVE)*/	{.fuel= 10, .gold= 0},
	/*ACTION(FUEL)*/	{.fuel=  1, .gold= 0},
	//~ /*ACTION(SMSH)*/	{.fuel= 10, .gold= 0},
	}
};
