//~ #define _POSIX_C_SOURCE 200809L
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <omp.h>
#include <stdarg.h>
#include <inttypes.h>
#include <math.h>
#include <tensorflow/c/c_api.h>



/*************************************/
// Types
/*************************************/
typedef		bool					Boo;
typedef		struct {}				Unit;
typedef		int32_t					Int32;
typedef		uint32_t				Nat32;
typedef		int64_t					Int64;
typedef		uint64_t				Nat64;
typedef		float					Flt32;
typedef		double					Flt64;
typedef		struct Pos32 {Int32 x; Int32 y;}	Pos32;
typedef		struct Dim32 {Nat32 w; Nat32 h;}	Dim32;
int itoa(int val, char* buf)
{
    const unsigned int radix = 10;

    char* p;
    unsigned int a;        //every digit
    int len;
    char* b;            //start of the digit char
    char temp;
    unsigned int u;

    p = buf;

    if (val < 0)
    {
        *p++ = '-';
        val = 0 - val;
    }
    u = (unsigned int)val;

    b = p;

    do
    {
        a = u % radix;
        u /= radix;

        *p++ = a + '0';

    } while (u > 0);

    len = (int)(p - buf);

    *p-- = 0;

    //swap
    do
    {
        temp = *p;
        *p = *b;
        *b = temp;
        --p;
        ++b;

    } while (b < p);

    return len;
}

/*************************************/
// Macros
/*************************************/
#define		MAX(a, b)				({__typeof__ (true ? a : b) _a = (a); __typeof__ (false ? a : b) _b = (b); _a > _b ? _a : _b;})
#define		MIN(a, b)				({__typeof__ (true ? a : b) _a = (a); __typeof__ (false ? a : b) _b = (b); _a < _b ? _a : _b;})
#define	throw(...)	_throw(__FILE__, __LINE__, __VA_ARGS__)
void _throw(char *filename, int line, char *format_string, ...) {
	va_list args;
	fprintf(stderr, "FATAL ERROR in file \"%s:%d\" : ", filename, line);
	va_start(args, format_string);
	vfprintf(stderr, format_string, args);
	va_end(args);
	fprintf(stderr, "\n");
	exit(1);
}

void* myalloc(Nat64 s) {
	//~ printf("Allocate %lu bytes.\n", s);
	return malloc(s);
}

//~ for(T *rng = RNG(Boo, 100), max = rng + 100; rng < max || {free(rng); false;}; rng++)
#define		GENERATOR				2
#define		RNG_Nat64()				((((Nat64)RNG_Nat32()) << 32) + RNG_Nat32())
#define		RNG(Typ)				((Typ) RNG_##Typ())
#define		RNGMOD(Typ, e)				(RNG_##Typ()%(e))

/*************************************/
// RNG Macros
/*************************************/
#if GENERATOR == 1

	#define		_RNG_					( (Int32) rand() )
	#define		RNG_Boo()				( (Boo) (rand() % 2) )
	#define		RNG_Int32()				( ((Int32)rand()) + (RNG_Boo() ? 1 << 31 : 0) )
	#define		RNG_Nat32()				( ((Nat32)rand()) + (RNG_Boo() ? 1 << 31 : 0) )

#endif

#if GENERATOR == 2

	#define		RNG_Int32()		((Int32)RNG_Nat32())

	Nat32* _rng_Nat32 = NULL;
	Nat32* _rng_Nat32_cur = NULL;
	Nat64 _rng_Nat32_siz = 100000000;
	Nat64 _rng_Nat32_len = 0;

	Nat32 RNG_Nat32() {
		//~ return (rand() + (rand() ? (1 << 31) : 0));
		//~ printf("IN Boo\n");
		if (!_rng_Nat32) {
			_rng_Nat32 = myalloc(sizeof(Nat32[_rng_Nat32_siz]));
			_rng_Nat32_cur = _rng_Nat32;
		}
		if (_rng_Nat32_cur == _rng_Nat32 + _rng_Nat32_len) {
			FILE* urandom = fopen("/dev/urandom", "r");
			if (!urandom)
				throw("Error opening /dev/urandom.");

			_rng_Nat32_len = fread(_rng_Nat32, 4, _rng_Nat32_siz, urandom);
			if (!_rng_Nat32_len || ferror(urandom)) {
				fclose(urandom);
				throw("Cannot read random number.");
			}
			fclose(urandom);
			_rng_Nat32_cur = _rng_Nat32;
			//~ printf("Read %lu Nat32 of random data.\n", _rng_Nat32_len);
		}
		//~ printf("OK Boo\n");
		return *_rng_Nat32_cur++;
	}

	Nat64 _rng_Boo_cur = 0;
	Nat32 _rng_Boo_bit = 64;
	Boo RNG_Boo() {
		throw("OK Boo\n");
		if (_rng_Boo_bit == 64) {
			_rng_Boo_cur = RNG_Nat64();
			_rng_Boo_bit = 0;
		}
		return _rng_Boo_cur & (1 << (_rng_Boo_bit++)) ? true : false;
	}

#endif



/*************************************/
// Classes
/*************************************/
#include "lib/lib.c"
#define		_TARGET_				"../Classes.inc.h"
#include						"lib/ObjectSystem.h"
#undef		_TARGET

/*************************************/
// Meta
/*************************************/

//~ #define		FMAP(F, XS)			XS
//~ #define		CONS(X, XS)			APPLY(F, (X, XS))

//~ #define	EVAL(...)			EVAL1(EVAL1(EVAL1(EVAL1(__VA_ARGS__))))
//~ #define EVAL1(...)			EVAL2(EVAL2(EVAL2(EVAL2(__VA_ARGS__))))
//~ #define EVAL2(...)			__VA_ARGS__

//~ # define EMPTY(...)
//~ # define DEFER(...) __VA_ARGS__ EMPTY()
//~ # define OBSTRUCT(...) __VA_ARGS__ DEFER(EMPTY)()

//~ #define HEAD(X, ...)			X , __VA_ARGS__
//~ #define FMAP(F, Op, PS, X, ...)		F(EXPLODE PS, X) Op
//~ #define	FMAPREC()			FMAP


//~ #define	MKELEM(K, E)			(1 << CONCAT(K, Tag_##E))
//~ #define SET(K, Op, Tags)		FMAP(MKELEM, Op, (K), HEAD Tags)

//~ #define	MKELEM(K, E)			(1 << CONCAT(K, Tag_##E))
//~ #define FMAPREC1(F, Op, PS, X, ...)	F(EXPLODE PS, X) __VA_OPT__(Op FMAPREC2(F, Op, PS, HEAD (__VA_ARGS__)))
//~ #define FMAP(F, Op, PS, X, ...)		F(EXPLODE PS, X) __VA_OPT__(Op APPLY(FMAP, (F, Op, PS, HEAD (__VA_ARGS__))))