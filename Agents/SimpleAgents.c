
S_Action RandomAgent(WorldState w, Cell c, AgentState a) {
	return Actions_Dad[rand() % 5];
}

S_Action SolutionAgent(WorldState w, Cell c, AgentState a) {
	return ACTION(MOVE, w->map->grps[(Nat32)(Nat64)a->data].soln[c - w->map->cells].dir);
	DirTag dirs[15];
	Nat32 ndirs = 0;
	Cell d;
	Policy* sol = w->map->grps[(Nat32)(Nat64)a->data].soln;
	for(DirTag i = DirTag_HERE; i <= DirTag_EAST; i++) {
		d = Cell_deltaTags(c, Dir_Data[i], CellTags_OPN);
		if (d && sol[d - w->map->cells].dst < sol[c - w->map->cells].dst )
			dirs[ndirs++] = i;
	}
	if (!ndirs) dirs[ndirs++] = DirTag_HERE;
	return ACTION(MOVE, dirs[RNGMOD(Nat32, ndirs)]);
}
