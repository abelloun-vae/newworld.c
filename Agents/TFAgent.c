
typedef struct MyTF*		MyTF;
typedef struct MyTFPool*	MyTFPool;

typedef struct MyTF {
	Nat32			szinp;
	Nat32			szout;
	TF_Status*		statu;
	TF_SessionOptions*	optns;
	TF_Graph*		graph;
	TF_Session*		sessn;
	TF_Output		opinp;
	TF_Output		opout;
	TF_Tensor*		dtinp;
	TF_Tensor*		dtout;
} S_MyTF;

typedef struct MyTFPool {
	Nat32			size;
	S_MyTF			modls[];
} S_MyTFPool;


void tf_save(MyTF mytf, char* dirpath) {
	TF_Buffer* buf = TF_NewBufferFromString("", 0);
	TF_SessionOptions* optns = TF_NewSessionOptions();
	TF_Graph* graph = TF_NewGraph();
	TF_Status* statu = TF_NewStatus();

	TF_Session* sessn = TF_LoadSessionFromSavedModel(optns, buf, dirpath, NULL, 0, graph, NULL, statu);
	if (TF_GetCode(statu) != TF_OK)
		throw("Error saving SavedModel: %s\n", TF_Message(statu));

	TF_DeleteBuffer(buf);
	TF_DeleteSessionOptions(optns);
	TF_DeleteGraph(graph);
	TF_DeleteStatus(statu);
	TF_CloseSession(sessn, statu);
	TF_DeleteSession(sessn, statu);
}

MyTF tf_init(MyTF mytf, char* dirpath, Nat32 outputSize, Int64 dims[]) {

	//~ MyTF mytf = malloc(sizeof(struct MyTF));
	*mytf = (struct MyTF) {
		.szinp	= dims[1],
		.szout	= outputSize,
		.statu	= TF_NewStatus(),
		.optns	= TF_NewSessionOptions(),
		.graph	= TF_NewGraph(),
		.dtinp	= TF_AllocateTensor(TF_FLOAT, dims, 2, sizeof(Flt32[dims[0] * dims[1]])),
		.dtout	= TF_AllocateTensor(TF_FLOAT, dims, 2, sizeof(Flt32[outputSize])),
	};

	// Load the SavedModel
	int num_tags = 1; const char* tags[] = {"serve"};
	TF_Buffer* buf = TF_NewBufferFromString("", 0);

	mytf->sessn = TF_LoadSessionFromSavedModel(mytf->optns, buf, dirpath, tags, num_tags, mytf->graph, NULL, mytf->statu);
	if (TF_GetCode(mytf->statu) != TF_OK)
		throw("Error loading SavedModel: %s\n", TF_Message(mytf->statu));

	mytf->opinp = (TF_Output) {TF_GraphOperationByName(mytf->graph, "serving_default_input_op"), 0};
	mytf->opout = (TF_Output) {TF_GraphOperationByName(mytf->graph, "StatefulPartitionedCall"), 0};

	//~ Nat64 pos = 0;
	//~ TF_Operation* op;
	//~ while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) )
		//~ printf("Input : %3lu => %s \n", pos, TF_OperationName(op));

	TF_DeleteBuffer(buf);
	return mytf;
}

void tf_free(MyTF mytf) {
	TF_DeleteSession(mytf->sessn, mytf->statu);
	TF_DeleteGraph(mytf->graph);
	TF_DeleteStatus(mytf->statu);
	TF_DeleteSessionOptions(mytf->optns);
	TF_DeleteTensor(mytf->dtinp);
	TF_DeleteTensor(mytf->dtout);
}



void tf_predict(MyTF mytf, Flt32 input[], Flt32* output) {
	memcpy(TF_TensorData(mytf->dtinp), input, sizeof(Flt32[mytf->szinp]));
	TF_SessionRun(mytf->sessn, NULL, &mytf->opinp, &mytf->dtinp, 1, &mytf->opout, &mytf->dtout, 1, NULL, 0, NULL, mytf->statu);
	if (TF_GetCode(mytf->statu) != TF_OK)
		throw("Error executing graph: %s\n", TF_Message(mytf->statu));
	memcpy(output, TF_TensorData(mytf->dtout), sizeof(Flt32[mytf->szout]));
}

//~ // Function to mutate the model by randomly changing weights and bias
//~ void tf_mutate2(MyTF mytf) {
	//~ // Get the graph associated with the session
	//~ TF_Graph *graph = mytf->graph;

	//~ size_t pos = 0;
	//~ TF_Operation* op;
	//~ while ((op = TF_GraphNextOperation(graph, &pos)) != NULL) {

		//~ // Get the number of outputs for the operation
		//~ int num_outputs = TF_OperationNumOutputs(op);

		//~ // Iterate through the values and randomly change weights and bias
		//~ for (int j = 0; j < num_outputs; j++) {
			//~ int shape_size;
			//~ // Get the size of the output shape
			//~ TF_Operation *op_output = TF_GraphOperationOutput(graph, op, j);
			//~ TF_Output output = TF_OperationOutputConsumers(op_output);
			//~ const int64_t *shape = TF_GraphGetTensorShape(graph, output, &shape_size, status);

			//~ // Generate random weights and bias
			//~ for (int k = 0; k < shape_size; k++) {
				//~ // Generate random float between 0 and 1
				//~ float random_float = (float)rand()/(float)(RAND_MAX);

				//~ // Set new weight and bias
				//~ TF_Output output = {op, j};
				//~ TF_SetTensor(graph, output, random_float, nullptr, 0, status);
			//~ }

		//~ }

	//~ }

	//~ // Check for errors
	//~ if (TF_GetCode(status) != TF_OK)
		//~ throw("Error mutating the graph: %s\n", TF_Message(status));

//~ }



//~ void tf_mutate(MyTF mytf) {
	//~ Nat64 pos = 0;
	//~ TF_Operation* op;
	//~ while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		//~ Nat32 nb_params = TF_OperationNumOutputs(op);
		//~ for (Nat32 i = 0; i < nb_params; i++) {
			//~ TF_DataType dtype = TF_OperationOutputType((TF_Output) {op, i});
			//~ if (dtype == TF_FLOAT) {
				//~ TF_Output output = {op, i};
				//~ TF_Tensor* tensor = TF_AllocateTensor(dtype, NULL, 0, 0);
				//~ TF_SessionGetTensor(mytf->sessn, output, tensor, mytf->statu);
				//~ if (TF_GetCode(mytf->statu) != TF_OK)
					//~ throw("Error getting tensor: %s\n", TF_Message(mytf->statu));
				//~ Flt32* data = TF_TensorData(tensor);
				//~ Nat32 size = TF_TensorByteSize(tensor) / sizeof(Flt32);
				//~ for (Nat32 j = 0; j < size; j++)
					//~ data[j] += (Flt32)rand() / RAND_MAX * 0.1 - 0.05;
				//~ TF_SessionRun(mytf->sessn, NULL, &output, &tensor, 1, NULL, NULL, 0, NULL, 0, NULL, mytf->statu);
				//~ if (TF_GetCode(mytf->statu) != TF_OK)
					//~ throw("Error setting tensor: %s\n", TF_Message(mytf->statu));
				//~ TF_DeleteTensor(tensor);
			//~ }
		//~ }
	//~ }
//~ }

void tf_mutate(MyTF mytf) {
	Nat64 pos = 0;
	TF_Operation* op;
	TF_Buffer* buf0 = TF_NewBufferFromString("", 0);
	TF_Buffer* buf1 = TF_NewBufferFromString("", 0);
	while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		Nat32 nb_params = TF_OperationNumOutputs(op);
		for (Nat32 i = 0; i < nb_params; i++) {
			TF_DataType dtype = TF_OperationOutputType((TF_Output) {op, i});
			if (dtype == TF_FLOAT) {
				TF_Output output = {op, i};
				TF_Tensor* tensor = TF_AllocateTensor(dtype, NULL, 0, 0);
                                TF_SessionRun(mytf->sessn, buf0, &output, &tensor, 1, NULL, NULL, 0, (const TF_Operation * const*) &op, 0, buf1, mytf->statu);
				//~ if (TF_GetCode(mytf->statu) != TF_OK)
					//~ throw("Error getting tensor: %s\n", TF_Message(mytf->statu));
				//~ Flt32* data = TF_TensorData(tensor);
				//~ Nat32 size = TF_TensorByteSize(tensor) / sizeof(Flt32);
				//~ for (Nat32 j = 0; j < size; j++)
					//~ data[j] += (Flt32)rand() / RAND_MAX * 0.1 - 0.05;
				//~ TF_SessionRun(mytf->sessn, NULL, &output, &tensor, 1, NULL, NULL, 0, NULL, 0, NULL, mytf->statu);
				//~ if (TF_GetCode(mytf->statu) != TF_OK)
					//~ throw("Error setting tensor: %s\n", TF_Message(mytf->statu));
				//~ TF_DeleteTensor(tensor);
			}
		}
	}
}


void tf_mutate2(MyTF mytf) {
	Nat64 pos = 0;
	TF_Operation* op;
	while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		Nat32 nb_params = TF_OperationNumOutputs(op);
		for (Nat32 i = 0; i < nb_params; i++) {
			TF_DataType dtype = TF_OperationOutputType((TF_Output) {op, i});
			if (dtype == TF_FLOAT) {
				TF_Output output = {op, i};
				TF_Tensor* tensor;// = TF_AllocateTensor(dtype, NULL, 0, 0);

                                // get the shape of the tensor
				Int32 dims;
				Int64 dim_sizes[8];
				dims = TF_GraphGetTensorNumDims(mytf->graph, output, mytf->statu);
				for(int k = 0; k < 8; k++) dim_sizes[k] = 1; //printf("dims : %li\n", dim_sizes[k]);
				printf("dims : %i\n", dims);
				if (TF_GetCode(mytf->statu) != TF_OK)
					throw("Error getting tensor rank: %s\n", TF_Message(mytf->statu));
				TF_GraphGetTensorShape(mytf->graph, output, dim_sizes, dims, mytf->statu);
				if (TF_GetCode(mytf->statu) != TF_OK)
					throw("Error getting tensor shape: %s\n", TF_Message(mytf->statu));

                                // Allocate the tensor
				tensor = TF_AllocateTensor(dtype, dim_sizes, dims, 0);

				// Set the tensor contents
				//~ Flt32* data = TF_TensorData(tensor);
				//~ Nat32 size = TF_TensorByteSize(tensor) / sizeof(Flt32);
				//~ for (Nat32 j = 0; j < size; j++)
					//~ data[j] += (Flt32)rand() / RAND_MAX * 0.1 - 0.05;

				//~ // Set the tensor in the graph
				//~ TF_SessionRun(mytf->sessn, NULL, &output, &tensor, 1, NULL, NULL, 0, NULL, 0, NULL, mytf->statu);
				//~ if (TF_GetCode(mytf->statu) != TF_OK)
					//~ throw("Error setting tensor: %s\n", TF_Message(mytf->statu));
				//~ TF_DeleteTensor(tensor);
			}
		}
	}
}



//~ void tf_mutate(MyTF mytf) {
	//~ Nat64 pos = 0;
	//~ TF_Operation* op;
	//~ while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		//~ Nat32 num_inps = TF_OperationNumInputs(op);
		//~ for (Nat32 i = 0; i < num_inps; ++i) {
			//~ TF_DataType type;
			//~ Nat32 num_dims;
			//~ Int64* dims;
			//~ Nat64 num_bytes;
			//~ void* data;
			//~ TF_OperationInputListGet(op, i, &type, &num_dims, &dims, &num_bytes, &data);
			//~ TF_OperationInput()
			//~ type = TF_OperationInputType(op);
			//~ if (type == TF_FLOAT) {
				//~ Flt32* flts = (Flt32*) data;
				//~ for (Nat32 j = 0; j < num_bytes/4; ++j) {
					//~ if (rand() % 100 < 5)
						//~ flts[j] += (rand() % 1000) / 1000.0f - 0.5f;
				//~ }
			//~ }
		//~ }
	//~ }
//~ }
//~ void tf_mutate(MyTF mytf) {
	//~ Nat64 pos = 0;
	//~ TF_Operation* op;
	//~ while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		//~ Nat32 nb_inputs = TF_OperationNumInputs(op);
		//~ Nat32 nb_outputs = TF_OperationNumOutputs(op);
		//~ if (nb_inputs > 0) {
			//~ for (Nat32 i = 0; i < nb_inputs; i++) {
				//~ TF_DataType dt = TF_OperationInputType(op, i);
				//~ if (dt == TF_FLOAT) {
					//~ Nat32 sz = TF_OperationInputListLength(op, i, mytf->statu);
					//~ if (TF_GetCode(mytf->statu) != TF_OK)
						//~ throw("Error getting input list length: %s\n", TF_Message(mytf->statu));
					//~ Flt32* data = TF_OperationInput(op, i);
					//~ for (Nat32 j = 0; j < sz; j++) {
						//~ if (randf() < 0.01)
							//~ data[j] += randf() - 0.5;
					//~ }
				//~ }
			//~ }
		//~ }
		//~ if (nb_outputs > 0) {
			//~ for (Nat32 i = 0; i < nb_outputs; i++) {
				//~ TF_DataType dt = TF_OperationOutputType(op, i);
				//~ if (dt == TF_FLOAT) {
					//~ Nat32 sz = TF_OperationOutputListLength(op, i, mytf->statu);
					//~ if (TF_GetCode(mytf->statu) != TF_OK)
						//~ throw("Error getting output list length: %s\n", TF_Message(mytf->statu));
					//~ Flt32* data = TF_OperationOutput(op, i);
					//~ for (Nat32 j = 0; j < sz; j++) {
						//~ if (randf() < 0.01)
							//~ data[j] += randf() - 0.5;
					//~ }
				//~ }
			//~ }
		//~ }
	//~ }
//~ }

//~ MyTF tf_mutate(MyTF mytf) {
	//~ MyTF newtf = malloc(sizeof(struct MyTF));
	//~ *newtf = *mytf;

	//~ // Copy the weights and bias from mytf
	//~ Nat64 pos = 0;
	//~ TF_Operation* op;
	//~ while ( (op = TF_GraphNextOperation(mytf->graph, &pos)) ) {
		//~ Nat32 n_inps = TF_OperationNumInputs(op);
		//~ Nat32 n_outs = TF_OperationNumOutputs(op);
		//~ for (Nat32 i = 0; i < n_inps; i++) {
			//~ TF_DataType type = TF_OperationInputType(op, i);
			//~ Nat64 sz = TF_TensorByteSize(TF_GraphOperationInput(mytf->graph, op, i));
			//~ TF_Tensor* tnsr = TF_AllocateTensor(type, NULL, 0, sz);
			//~ memcpy(TF_TensorData(tnsr), TF_TensorData(TF_GraphOperationInput(mytf->graph, op, i)), sz);
			//~ TF_GraphSetTensor(newtf->graph, TF_OperationInput(op, i), tnsr);
		//~ }
		//~ for (Nat32 i = 0; i < n_outs; i++) {
			//~ TF_DataType type = TF_OperationOutputType(op, i);
			//~ Nat64 sz = TF_TensorByteSize(TF_GraphOperationOutput(mytf->graph, op, i));
			//~ TF_Tensor* tnsr = TF_AllocateTensor(type, NULL, 0, sz);
			//~ memcpy(TF_TensorData(tnsr), TF_TensorData(TF_GraphOperationOutput(mytf->graph, op, i)), sz);
			//~ TF_GraphSetTensor(newtf->graph, TF_OperationOutput(op, i), tnsr);
		//~ }

		//~ // Randomly modify the weights and bias with a small probability
		//~ Flt32* data = (Flt32*) TF_TensorData(TF_GraphOperationOutput(newtf->graph, op, 0));
		//~ for (Nat32 i = 0; i < sz; i++) {
			//~ if (rand() % 100 < 5)
				//~ data[i] += (rand() % 100) / 100.0;
		//~ }
	//~ }

	//~ return newtf;
//~ }

Nat64 sizeof_MyTFPool(Nat32 size) {
	return sizeof(S_MyTFPool) + sizeof(S_MyTF[size]);
}

MyTFPool init_MyTFPool(MyTFPool pool, Nat32 size) {
	char* dirpath = "/home/dader/Work/Keras/Agents/agent_model_";
	char ndir[100]; char* rr = dirpath; Nat32 q;
	for(q = 0; dirpath[q]; q++) ndir[q] = dirpath[q];
	pool->size = size;
	for(Nat32 i = 0; i < size; i++) {
		itoa(i, ndir + q);
		printf("%s\n", ndir);
		tf_init(pool->modls + i, ndir, 8, (Int64[]) {1, 46});
		//~ if ( !i ) {
			//~ tf_mutate(pool->modls + i);
			//~ throw("ok");
		//~ }
	}
	return pool;
}


S_Action SmartShellAgent(WorldState w, Cell c, AgentState g, MyTF mytf) {

	//~ return ACTION(MOVE, RNGMOD(Nat32, 4) + 1);

	Cell d, o;
	Nat32 wi = w->map->conf->dim.w;
	Nat32 hi = w->map->conf->dim.h;
	MapState map;

	if ( !g->data )
		g->data = NEW(MapState, (w->map->conf));

	map = g->data;
	if ( !w->tick ) {
		init_MapState(map, w->map->conf);
		for(Nat32 i = 0, imax = MAPSIZE(w->map); i < imax; i++)
			Cell_reset(map->cells + i, CellTag_UNKW, (Pos32) {y: i / wi, x: i % wi});
	}


	for(Int32 j = -1; j <= 1; j++) for(Int32 i = -1; i <= 1; i++) {
		d = &map->cells[(c->pos.y + j) * wi + c->pos.x + i];
		o = &w->map->cells[(c->pos.y + j) * wi + c->pos.x + i];
		if ( d->tag == CellTag_UNKW ) {
			Cell_set(d, o);
			if ( Cell_Data[o->tag] ) {
				map->grps[Cell_Data[o->tag]].updt = true;
				g->scor.score += 10;
			}
		}
		d->mrk = o->mrk;
		d->fuel = o->fuel;
		d->gold = o->gold;
		d->tkns = o->tkns;
		//~ d->pop = o.pop;
	}

	Flt32 vector[46]; Nat32 i = 0; //for(int j = 0; j < 46; j++) vector[j] = 0;
	vector[i++] = ((Flt32) g->fuel) / ((Flt32) w->conf->AGENT_FUEL_MAX); //fuel
	for(Int32 y = -1; y <= 1; y++)
		for(Int32 x = -1; x <= 1; x++) {
			d = c + y * (Int64) wi + x;
			vector[i++] = !d->mrk ? 1 : 0;
			vector[i++] = d->tkns ? 1 : 0;
			vector[i++] = d->tag == CellTag_VOID ? 1 : 0;
			vector[i++] = d->tag == CellTag_WALL ? 1 : 0;
			vector[i++] = d->tag == CellTag_FUEL ? 1 : 0;
		}

	Flt32 ans[8] = {}; Nat32 u = 0;
	tf_predict(mytf, vector, ans);

	//~ printf("(");
	//~ for(int i = 0; i < 8; i++) printf("%f,", ans[i]);
	//~ printf(")\n");


	Flt32 m = 1000;
	Nat32 xxx = 0, xxs[8];
	for(Nat32 i = 0; i < 8; i++) {
		xxs[i] = (Nat32) (Int64) (m * MAX(MIN(ans[i], 1), 0));
		xxx += xxs[i];
	}

	Nat32 j = 0;
	for(Nat32 xxz = RNGMOD(Nat32, xxx); xxz >= xxs[j]; xxz -= xxs[j++]);



	//~ for(Nat32 i = 0; i < 8; i++)
		//~ if ( ans[i] > ans[u] )
			//~ u = i;



	//~ printf("RESULT : %f %f %f %f %f %f %f %f \n", ans[0], ans[1], ans[2], ans[3], ans[4], ans[5], ans[6], ans[7]);
	//~ if (u) printf("Output tensor data = %f\n", ans[u]);
	Boo show = false;
	switch(j) {
		case 1:		if(show) printf("MARK 0\n"); return ACTION(MARK, false);
		case 2:		if(show) printf("MARK 1\n"); return ACTION(MARK, true);
		case 3:		if(show) printf("MOVE 0\n"); return ACTION(MOVE, DirTag_NORT);
		case 4:		if(show) printf("MOVE 1\n"); return ACTION(MOVE, DirTag_SOUT);
		case 5:		if(show) printf("MOVE 2\n"); return ACTION(MOVE, DirTag_WEST);
		case 6:		if(show) printf("MOVE 3\n"); return ACTION(MOVE, DirTag_EAST);
		case 7:		if(show) printf("FUEL\n"); return ACTION(FUEL, w->conf->AGENT_FUEL_MAX - g->fuel);
		default:	if(0) printf("WAIT\n"); return ACTION(WAIT);
	}

	//~ return ACTION(WAIT);
}
