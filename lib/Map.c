/****************************************/
// Map system
/****************************************/
Nat64 sizeof_MapState(MapConf m) {
	Nat32 N = m->dim.h * m->dim.w;
	return sizeof(S_MapState) + sizeof(S_Cell[N]) + sizeof(Cell[N]);
}

MapState init_MapState(MapState map, MapConf conf) {
	Nat32 N = conf->dim.h * conf->dim.w;
	map->conf = conf;
	map->grav = NULL;
	map->hots = NULL;
	map->stck.size = MAPSIZE(map);
	map->stck.cells = (Cell*)(((char*)map) + sizeof(S_MapState) + sizeof(S_Cell[N]));
	for(GrpTag i = 0; i < _GrpTags_; i++)
		map->grps[i] = (CellGroup) {
			.updt = false,
			.size = 0,
			.elms = NULL,
			.head = NULL,
			.soln = NULL,
		};

	map->tkns = (CellGroup) {
		.updt = false,
		.size = 0,
		.elms = NULL,
		.head = NULL,
		.soln = NULL,
	};

	//~ map->conf->reset(map);
	return map;
}

/****************************************/
// Map
/****************************************/
Cell Map_randomCellCb(MapState map, Boo(callback)(Cell)) {
	Nat32 n = 1000;
	for(Nat32 r, i = 0, z = MAPSIZE(map); i < n; i++) {
		r = RNG(Nat32) % z;
		//~ if ((1 << map->cells[r].tag) & tags)
		if (callback(map->cells + r))
			return &map->cells[r];
	}
	_throw(__FILE__, __LINE__, "Can't find a random cell with callback after %u tries.\n", n);
	return NULL;
}

Cell Map_randomCellTags(MapState map, CellTags tags) {

	Nat32 n = 1000;
	Nat32 count = 0, counts[_GrpTags_]; for(GrpTag gt = 0; gt < _GrpTags_; gt++) counts[gt] = 0;

	for (CellTag t = 0; t < _CellTags_; t++) if ( (1 << t) & tags )
		for (Cell c = map->grps[Cell_Data[t]].head; c; c = c->grp)
			if ( (1 << c->tag) & tags ) {
				count++;
				counts[Cell_Data[t]]++;
			}

	if ( !count ) { // We're in the case of looking for a non indexed cell
		for(Nat32 r, i = 0, z = MAPSIZE(map); i < n; i++) {
			r = RNG(Nat32) % z;
			if ((1 << map->cells[r].tag) & tags)
				return &map->cells[r];
		}
		throw("Can't find a random cell with tags %u after %u tries.\n", tags, n);
	}

	Nat32 r = RNGMOD(Nat32, count);
	for(GrpTag gt = 0; gt < _GrpTags_; r -= counts[gt++])
		if ( r < counts[gt] )
			for (Cell c = map->grps[gt].head; c; c = c->grp)
				if ( !r-- )
					return c;

	throw("Should not happen.\n");
	return NULL;
}

Cell Map_randomCell(MapState map) {
	return Map_randomCellTags(map, CellTags_CELL);
}


Boo Map_findCellsTags(MapState map, CellQ list, CellTags tags) {
	Nat32 count = 0;
	for(CellTag t = 0; t < _CellTags_; t++) if ( (1 << t) & tags )
		for(Cell *queue = list->cells, c = map->grps[Cell_Data[t]].head; c; c = c->grp)
			if (c->tag == t)
				queue[count++] = c;
	list->count = count;
	return count > 0;
}

Boo Map_findCells(MapState map, CellQ list, Boo(callback)(Cell)) {
	Nat32 count = 0;
	for(Cell *queue = list->cells, c = map->cells, max = c + MAPSIZE(map); c < max; c++)
		if (callback(c))
			queue[count++] = c;
	list->count = count;
	return count > 0;
}


Cell Map_respawn(MapState map, AgentState a) {

	if (map->conf->RESPAWN)
		return Map_randomCellTags(map, map->conf->RESPAWN);

	CellGroup g = map->grps[GRP(STRT)];
	if (!g.size)
		throw("No STRT cell found for the deterministic distribution respawn strategy.");
	Nat32 x = a->id % g.size;
	for(Cell c = g.head; c; c = c->grp)
		if (!x--) return c;
	return NULL;
}



void Map_stepEffect(MapState map, Cell c, AgentState a) {
	switch(c->tag) {
		case CellTag_TRAP:
			a->fuel -= MIN(a->fuel, map->conf->TRAP_FUEL_PENALTY);
			a->gold -= MIN(a->gold, map->conf->TRAP_GOLD_PENALTY);
		break;
		default: break;
	}
}


//############ PROCESSING ###############
void Map_step(MapState map, Nat32 tick) {

	//~ MATRIX(m, map);
	//~ Nat32 w = map->width;
	//~ Nat32 h = map->height;

	//~ Nat64 yy = 0;
	//~ for(Cell c = map->grps[0], c0 = c ? c->grp : NULL; c; c = c0, c0 = c ? c->grp : NULL)
		//~ switch(c->tag) {
			//~ case CellTag_QBIT: Cell_stepQBIT(c, tick, map); break;
			//~ case CellTag_CLCK: Cell_stepCLCK(c, tick, map); break;
			//~ case CellTag_FUEL: Cell_stepFUEL(c, tick, map); break;
			//~ case CellTag_ORCL: Cell_stepORCL(c, tick, map); break;
			//~ case CellTag_TRAP: Cell_stepTRAP(c, tick, map); break;
			//~ default: throw("Cannot process cell %s in workgroup.", Cell_Names[c->tag]);
		//~ }

	//~ Cell* prev = &map->pltfGroup;
	//~ for(Cell c = map->pltfGroup, nxt = c->grp; c; c = nxt, nxt = c ? c->grp : NULL) {
		//~ Nat32 x = c->pos.x;
		//~ Nat32 y = c->pos.y;
		//~ DirTag dir = c->data.pltf.dexpr(map, c, tick);
		//~ c->data.pltf.dir = dir ?: c->data.pltf.dir;
		//~ switch(dir) {
			//~ case DirTag_NORT:
				//~ y--;
			//~ break;
			//~ case DirTag_SOUT:
				//~ y++;
			//~ break;
			//~ case DirTag_EAST:
				//~ x++;
			//~ break;
			//~ case DirTag_WEST:
				//~ x--;
			//~ break;
			//~ case DirTag_NONE:
				//~ prev = &c->group;
				//~ continue;
		//~ }

		//~ CellState d = &(*m)[y][x];
		//~ *d = *c;
		//~ d->pos = (Pos) {x: x, y: y};
		//~ d->prev = NULL;
		//~ d->next = NULL;
		//~ c->tag = CellTag_VOID;
		//~ c->tokens = 0;
		//~ c->agents = NULL;
		//~ if ( c->pop ) {
			//~ c->pop = 0;
			//~ Cell_update(c);
			//~ Cell_update(d);
			//~ for(AgentState a = d->agents; a; a = a->next)
				//~ a->pos = d->pos;
		//~ }

		//~ *prev = &(*m)[y][x];
		//~ prev = &(*m)[y][x].group;

	//~ }

}












//~ void Cell_forRect(Cell tl, Cell br, Cell(callback)(Cell, Cell), Cell data) {
	//~ MapState map = Cell_getMap(tl);
	//~ for(Int32 y = tl->pos.y; y <= br->pos.y; y++)
		//~ for(Int32 x = tl->pos.x; x <= br->pos.x; x++)
			//~ callback(MAP(map, x, y), data);
//~ }


//~ void Map_forRandomCellTag(MapState map, CellTag tag, Nat32 count, void (callback)(CellState, void*), void* data) {
	//~ for (int i = 0; i < count; i++)
		//~ callback(Map_randomCellTag(map, tag), data);
//~ }

//~ void Map_forRandomCell(MapState map, Nat32 count, void (callback)(CellState, void*), void* data) {
	//~ Map_forRandomCellTag(map, CellTag_CELL, count, callback, data);
//~ }





