
void Map_improvePath(MapState map, Policy* policy, CellQ list, Cell tgt) {

	DirTag  moves[] = {DirTag_NORT, DirTag_SOUT, DirTag_WEST, DirTag_EAST};
	DirTag imoves[] = {DirTag_SOUT, DirTag_NORT, DirTag_EAST, DirTag_WEST};

	Nat32 j = 0;
	for (Cell *queue = list->cells, *end = queue + list->count, *max = queue + list->size, d;
		queue != end && *queue != tgt; queue = queue + 1 < max ? queue + 1 : list->cells) {
		//~ if (j++ % 1 == 100) {
			//~ PRINT(MapSolution, map, policy);
			//~ usleep(100000);
		//~ }
		for (Nat32 i = 0, rnd; i < 4; i++)
			if ( (d = Cell_deltaTags(*queue, Dir_Data[moves[i]], CellTags_OPN)) )
				if (policy[*queue - map->cells].dst + 1 < policy[d - map->cells].dst) {

					policy[d - map->cells].dst = policy[*queue - map->cells].dst + 1;
					policy[d - map->cells].dir = imoves[i];

					if (!policy[d - map->cells].stk) {
						policy[d - map->cells].stk = true;
						rnd = RNGMOD(Nat32, (end - queue + list->size) % list->size) + 1;
						*end++ = list->cells[(queue - list->cells + rnd) % list->size];
						list->cells[(queue - list->cells + rnd) % list->size] = d;
						//~ *end++ = d;
						if (end == max)
							end = list->cells;
					}

				}

		policy[*queue - map->cells].stk = false;

	}

}


void Map_compute(MapState map, Policy* policy, CellQ list, Cell tgt) {
	for(Nat32 i = 0, N = MAPSIZE(map); i < N; i++)
		policy[i] = (Policy) {dst: -1 /* ie MAX_Nat32 */, dir: DirTag_HERE, stk: false};
	for(Nat32 i = 0, count = list->count; i < count; i++) {
		policy[list->cells[i] - map->cells].dst = 0;
		policy[list->cells[i] - map->cells].stk = true;
	}
	Map_improvePath(map, policy, list, tgt);
}


//~ Policy* Map_solvePath(Cell src, Cell tgt) {
	//~ MapState map = Cell_getMap(src);
	//~ Nat32 N = MAPSIZE(map);
	//~ Nat64 size = sizeof(Policy[N]) + sizeof(Cell[N]);
	//~ Policy* policy = malloc(size);
	//~ printf("Allocate %lu bytes for path solution.\n", size);
	//~ Cell* queue = (Cell*) (policy + N);
	//~ queue[0] = src;
	//~ Map_compute(map, policy, queue, 1, tgt);
	//~ return policy;
//~ }


void Map_solve(MapState map) {

	Boo hasTokens(Cell c) {
		return c->tkns > 0;
	}

	Nat32 N = MAPSIZE(map);
	Nat64 size = (_GrpTags_ + 1) * sizeof(Policy[N]);

	char* mem = ALLOC(size);
	//~ printf("Allocate %lu bytes for map solution.\n", size);
	map->tkns.soln = (Policy*) mem;
	for(GrpTag t = 0; t < _GrpTags_; t++)
		map->grps[t].soln = (Policy*) (mem + (t + 1) * sizeof(Policy[N]));

	//~ // get the tokens
	Map_findCells(map, &map->stck, &hasTokens);
	Map_compute(map, map->tkns.soln, &map->stck, NULL);
	//~ printf("Showing solution for tokens (%u cells).\n", map->stck.count);
	//~ PRINT(MapSolution, map, map->tkns.soln);

	for(GrpTag t = 0; t < _GrpTags_; t++) {
		Nat32 i = 0;
		for(Cell c = map->grps[t].head; c; c = c->grp)
			map->stck.cells[i++] = c;
		map->stck.count = i;
		Map_compute(map, map->grps[t].soln, &map->stck, NULL);
		//~ printf("Showing solution for group : %s(%u cells).\n", Grp_Names[t], map->grps[t].size);
		//~ PRINT(MapSolution, map, map->grps[t].soln);
	}

}
















