

/****************************************/
// Cells
/****************************************/
//~ #define TAG(Name, DataValue)
//~ #define TAGS(Data, Tags)
//~ #define CLASS(Name, Type)
//~ #define TYPE(Props, Classes)
//~ #include "./Map.def.h"


MapState Cell_getMap(Cell c) {
	return OFFSET(MapState, cells, c - ((c - c->pos.x - 1)->pos.x + 1) * c->pos.y - c->pos.x);
}

Nat32 Cell_getWidth(Cell c) {
	return c->pos.y ? (Nat32) ((c - c->pos.x - 1)->pos.x + 1) : OFFSET(MapState, cells, c - c->pos.x)->conf->dim.w;
}

Cell Cell_reset(Cell c, CellTag tag, Pos32 pos) {
	*c = (S_Cell) {
		tag: tag,
		mrk: false,
		pos: pos,
		pop: 0,
		tkns: 0,
		fuel: 0,
		gold: 0,
		prv: NULL,
		nxt: NULL,
		grp: NULL,
		ags: NULL,
	};
	return c;
}

Cell Cell_set(Cell c, Cell data) {
	c->tag = data->tag;
	c->data = data->data;
	GrpTag grp = Cell_Data[c->tag];
	if (grp) {
		MapState m = Cell_getMap(c);
		c->grp = m->grps[grp].head;
		m->grps[grp].head = c;
		m->grps[grp].size++;
	}
	return c;
}



Cell Cell_delta(Cell c, Pos32 dlt) {
	MapState m = Cell_getMap(c);
	Int32 x = c->pos.x + dlt.x;
	if ( x < 0 || x >= (Int32) m->conf->dim.w )
		return NULL;
	Int32 y = c->pos.y + dlt.y;
	if ( y < 0 || y >= (Int32) m->conf->dim.h )
		return NULL;
	return MAP(m, x, y);
}

Cell Cell_deltaTags(Cell c, Pos32 dlt, CellTags tags) {
	Cell z = Cell_delta(c, dlt);
	return (z && (1 << z->tag) & tags) ? z : NULL;
}

//############ PROCESSING ###############
void Cell_update(Cell c) {
	MapState m = Cell_getMap(c);
	if (c->pop) {

		c->nxt = m->hots;
		if (c->nxt)
			c->nxt->prv = c;
		m->hots = c;

	} else {

		if (c->prv)
			c->prv->nxt = c->nxt;
		else m->hots = c->nxt;

		if (c->nxt)
			c->nxt->prv = c->prv;

		c->prv = NULL;
		c->nxt = NULL;

	}
}


void Cell_addAgent(Cell c, AgentState a) {

	a->pos = c->pos;
	a->next = c->ags;

	if (a->next)
		a->next->prev = a;

	c->ags = a;

	c->pop++;
	if (c->pop == 1)
		Cell_update(c);
}

void Cell_remAgent(Cell c, AgentState a) {

	if (a->prev)
		a->prev->next = a->next;
	else c->ags = a->next;

	if (a->next)
		a->next->prev = a->prev;

	a->next = NULL;
	a->prev = NULL;

	c->pop--;
	if (!c->pop)
		Cell_update(c);

}


void Cell_kilAgent(Cell c, AgentState a) {

	Cell_remAgent(c, a);

	MapState map = Cell_getMap(c);

	if (a->tokn)
		c->tkns++;

	if (a->fuel)
		c->fuel = MIN(map->conf->CELL_FUEL_MAX, c->fuel + a->fuel);

	a->next = map->grav;
	if ( a->next ) a->next->prev = a;
	map->grav = a;

}
