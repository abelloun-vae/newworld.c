/*************************************/
// Minilib
/*************************************/

//~ Nat64 npow(Nat64 b, Nat64 n) {
	//~ Nat64 r = 1;
	//~ for(Nat64 i = 0; i < n; i++)
		//~ r *= b;
	//~ return r;
//~ }

//~ Nat64 nlog(Nat64 n) {
	//~ for(Nat32 i = 0; i < 32; i++)
		//~ if (n & (1 << i))
			//~ return i;
	//~ return 0;
//~ }

//~ void print_current_time_with_ms (void)
//~ {
    //~ long            ms; // Milliseconds
    //~ time_t          s;  // Seconds
    //~ struct timespec spec;

    //~ clock_gettime(CLOCK_REALTIME, &spec);

    //~ s  = spec.tv_sec;
    //~ ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
    //~ if (ms > 999) {
        //~ s++;
        //~ ms = 0;
    //~ }

    //~ printf("Current time: %"PRIdMAX".%03ld seconds since the Epoch\n",
           //~ (intmax_t)s, ms);
//~ }