
Cell Action_process(WorldState w, Cell c, AgentState g) {
	switch(g->actn.tag) {

		case ActionTag_WAIT:	return Action_processWAIT(w, c, g, g->actn.data.WAIT);
		case ActionTag_MARK:	return Action_processMARK(w, c, g, g->actn.data.MARK);
		case ActionTag_MOVE:	return Action_processMOVE(w, c, g, g->actn.data.MOVE);
		case ActionTag_FUEL:	return Action_processFUEL(w, c, g, g->actn.data.FUEL);
		//~ case ActionTag_PICK:	return Action_processTOKN(w, c, g);
		//~ case ActionTag_DROP:	return Action_processTOKN(w, c, g);

		//~ case ActionTag_HELP:
			//~ CellState d = Cell_deltaTag(c, DirDeltas[g->action.dat.help.dir], CellTag_TRAP);
			//~ if (!d) return ResultTag_FAIL;
			//~ d->data.trap.open = true;
			//~ return ResultTag_DONE;

		//~ case ActionTag_MARK:
			//~ c->mrk = g->action.dat.mark.mrk;
			//~ return ResultTag_DONE;

		//~ case ActionTag_FLAG:
			//~ c->data.flag.on = g->action.dat.flag.flg;
			//~ return ResultTag_DONE;

		//~ case ActionTag_BCST:
			//~ w->com->box[w->com->size++] = (S_Message) {
				//~ tag: g->action.dat.bcst.msg,
				//~ dlt: g->action.dat.bcst.dlt,
				//~ pos: c->pos,
			//~ };
			//~ return ResultTag_DONE;

		default:
			//~ throw("ERROR in processing unknown action %s !\n", Action_Names[g->action.tag]);
			throw("ERROR in processing unknown action %u !\n", g->actn.tag);
		return NULL;

	}

}

