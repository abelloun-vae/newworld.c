#if SECTION == HEADERS

	#define	RECT(V, c1, c2)\
		for(Cell _c1 = (c1), _c2 = (c2); _c1; _c1 = NULL)\
		for(Int32 y = 0, ymax = _c2->pos.y - _c1->pos.y, w = Cell_getWidth(_c1); y <= ymax; y++)\
		for(Cell c = _c1 + y * w, c0 = c + _c2->pos.x - _c1->pos.x; c <= c0; c++)

	#define CELL(T, ...)		(&CONSTRUCTOR(Cell, T, __VA_ARGS__))
	#define MAPSIZE(map)		({__typeof__ (map) _map = (map); _map->conf->dim.w * _map->conf->dim.h;})
	#define MATRIX(map, mat)	S_Cell (*mat)[(map)->conf->dim.w] = (S_Cell(*)[(map)->conf->dim.w]) map->cells
	#define PAMID(map, pos)		({__typeof__ (pos) _pos = (pos); _pos.y * (map)->conf->dim.w + _pos.x;})
	#define PAM(map, pos)		({__typeof__ (map) _map = (map); &_map->cells[PAMID(_map, pos)];})
	#define MAPID(map, x, y)	PAMID(map, ((Pos32) {x, y}))
	#define MAP(map, x, y)		PAM(map, ((Pos32) {x, y}))

	#define GRP(grp)		GrpTag_##grp

	typedef		Nat32				CId;
	typedef		struct Cell*			Cell;
	typedef		struct CellQ*			CellQ;
	typedef		struct MapConf*			MapConf;
	typedef		struct MapState*		MapState;
	typedef		struct MapSolution*		MapSolution;
	typedef		enum DirTag			(*DExpr)(MapState m, Cell c, Nat64 tick);
	typedef		Boo				(*BExpr)(MapState m, Cell c, Nat64 tick);

#endif



#define CONTEXT Dir
	TAGS(Pos32, (
		TAG(HERE, ({ 0,  0}))
		TAG(NORT, ({ 0, -1}))
		TAG(SOUT, ({ 0,  1}))
		TAG(WEST, ({-1,  0}))
		TAG(EAST, ({ 1,  0}))
		TAG(NOWE, ({-1, -1}))
		TAG(NOEA, ({ 1, -1}))
		TAG(SOWE, ({-1,  1}))
		TAG(SOEA, ({ 1,  1}))
	))
#undef CONTEXT


#define CONTEXT Grp
	TAGS(Unit, (
		TAG(NONE, ({}))
		TAG(STRT, ({}))
		TAG(GOAL, ({}))
		TAG(SINK, ({}))
		TAG(WORK, ({}))
		TAG(PLTF, ({}))
	))
#undef CONTEXT



#define CONTEXT Cell

	#define METHODS(T, step)	void Cell_step##T(Cell this, Nat32 tick, MapState map) step

	TYPE(GrpTag,
		(
			Boo		mrk;	// Boo: 1
			Pos32		pos;
			Nat32		pop;	// ??	: pop / totalPop + aliv / totalPop
			Nat32		tkns;	// ??	: Boo + tkns / localPop
			Nat64		fuel;	// ??	: 0-fuel/maxfuel : 1 flt neuron + 1urg when dst(me, fuel) + 1 == me.fuel
			Nat64		gold;	// 0-32 : 5 neurons
			Cell		prv;
			Cell		nxt;
			Cell		grp;
			AgentState	ags;	// ??
		),
		(
			CLASS(VOID, (GRP(NONE)), Unit)
			CLASS(WALL, (GRP(NONE)), Unit)
			CLASS(CELL, (GRP(NONE)), Unit)
			CLASS(STRT, (GRP(STRT)), Unit)
			CLASS(GOAL, (GRP(GOAL)), Unit)
			CLASS(SINK, (GRP(SINK)), Unit)

			CLASS(FUEL, (GRP(WORK)), struct {Nat64 rate; Nat64 incr;}, ({
				this->fuel = MIN(map->conf->CELL_FUEL_MAX, this->data.FUEL.rate * this->fuel + this->data.FUEL.incr);
			}))

			CLASS(GOLD, (GRP(WORK)), struct {Nat64 rate; Nat64 incr;}, ({
				this->gold = MIN(map->conf->CELL_GOLD_MAX, this->data.GOLD.rate * this->gold + this->data.GOLD.incr);
			}))

			CLASS(TRAP, (GRP(WORK)), Boo, ({
				if ( !this->pop && !this->data.TRAP )
					this->data.TRAP = true;
			}))

			CLASS(HOVR, (GRP(NONE)), Unit)

			CLASS(FLAG, (GRP(NONE)), Boo)

			CLASS(QBIT, (GRP(WORK)), Boo, ({
				this->data.QBIT = RNG(Boo);
			}))

			CLASS(CLCK, (GRP(WORK)), struct {Boo on; Nat32 period;}, ({
				if (tick % this->data.CLCK.period == 0)
					this->data.CLCK.on = !this->data.CLCK.on;
			}))

			CLASS(ORCL, (GRP(WORK)), struct {Boo on; BExpr bexpr;}, ({
				this->data.ORCL.on = this->data.ORCL.bexpr(map, this, tick);
			}))
			CLASS(UNKW, (GRP(NONE)), Unit)
		)
	)

	#if SECTION == PRELUDE
		Cell		Cell_delta(Cell, Pos32);
		void		Cell_addAgent(Cell, AgentState);
		void		Cell_remAgent(Cell, AgentState);
		Cell		Map_randomCell(MapState map);
		Cell		Map_randomCellCb(MapState map, Boo(callback)(Cell));
		Boo		Map_findCellsTags(MapState map, CellQ list, CellTags tags);
		Boo		Map_findCells(MapState map, CellQ list, Boo(callback)(Cell));
		Cell		Map_randomCellTags(MapState map, CellTags tags);
		Cell		Map_respawn(MapState map, AgentState a);
		CellTags	CellTags_OPN = ~(CellTags_VOID | CellTags_WALL);
	#endif

	#undef METHODS

#undef CONTEXT



#if SECTION == STRUCTS

	typedef struct CellQ {
		Nat32		size;
		Nat32		count;
		Cell*		cells;
	} S_CellQ;

	typedef struct Policy {
		DirTag		dir;
		Nat32		dst;
		Boo		stk;
	} Policy;

	typedef struct MapConf {
		MapState	(*reset)(MapState);
		Dim32		dim;
		CellTags	RESPAWN;
		Nat64		CELL_FUEL_START;
		Nat64		CELL_FUEL_MAX;
		Nat64		CELL_GOLD_START;
		Nat64		CELL_GOLD_MAX;
		Nat64		TRAP_FUEL_PENALTY;
		Nat64		TRAP_GOLD_PENALTY;
	} S_MapConf;

	typedef struct CellGroup {
		Boo		updt;
		Nat32		size;
		Cell*		elms;
		Cell		head;
		Policy*		soln;
	} CellGroup;

	typedef struct MapState {
		MapConf		conf;
		AgentState	grav;
		Cell		hots;
		S_CellQ		stck;
		CellGroup	tkns;
		CellGroup	grps[_GrpTags_];
		S_Cell		cells[];
	} S_MapState;

#endif



			//~ CLASS(INFO, struct {MsgTag msg; Pos32 dlt;})




			//~ CLASS(PLTF, struct {
				//~ DirTag	dir;
				//~ DExpr	dexpr;
				//~ Nat64	state;
			//~ })

			//~ DATA(BARR, struct {Boo open;})
			//~ DATA(PASS, struct {Boo open;})
			//~ DATA(ROCK, struct {Nat64 fuel; Nat64 gold;})


	//~ #define DIR(D)			dir: DirTag_##D
//~ SET(Cell, (WALL, VOID))

//~ /** Agent Signaling cells **/
//~ CellTag_HOVR = 0x00000400,		// '=', active when agents are on it
//~ CellTag_FLAG = 0x00000800,		// '+-', can be set up/down by agents

//~ /** Actuator cells **/
//~ CellTag_ROOM = 0x00001000,		// ':�', open state depends on some BExpr
//~ CellTag_PLTF = 0x00002000,		// '_', plateform position depends on some PExpr
//~ CellTag_INFO = 0x00004000,		// '?', info cells, contains a message

//~ CellTag_ROCK = 0x00008000,		// '', when smashed enough times, turns into a cell, a fuel or a mine
//~ CellTag_MINE = 0x00010000,		// '$', gold mine
//~ CellTag_GATE = 0x00020000,		// '', access to this cell requires an access fee

//~ CellTag CellTag_ANY	= ~0;
//~ CellTag CellTag_CLS	= CellTag_WALL | CellTag_VOID;
//~ CellTag CellTag_OPN	= ~(CellTag_WALL | CellTag_VOID);
//~ CellTag CellTag_DGR	= CellTag_TRAP | CellTag_WALL | CellTag_VOID;
//~ CellTag CellTag_SAF	= ~(CellTag_TRAP | CellTag_WALL | CellTag_VOID);



//~ //#######################
//~ //####### Cells #########
//~ //#######################

//~ typedef struct CellData_FLAG {
	//~ Boo		on;
//~ } S_CellData_FLAG;

//~ typedef struct CellData_ROOM {
	//~ Boo		open;
	//~ BExpr		bexpr;
//~ } S_CellData_ROOM;

//~ typedef struct CellData_PLTF {
	//~ DirTag		dir;
	//~ DExpr		dexpr;
	//~ Nat64		state;
//~ } S_CellData_PLTF;

//~ typedef struct CellData_INFO {
	//~ MsgTag		msg;
	//~ Delta		dlt;
//~ } S_CellData_INFO;
