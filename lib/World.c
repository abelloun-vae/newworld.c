/****************************************/
// Constructor
/****************************************/
Nat64 sizeof_WorldState(WorldConf conf) {
	return	sizeof(S_WorldState) +
		sizeof(S_Message[conf->size]) +
		sizeof(AgentState[2][conf->size]) +
		sizeof(S_AgentState[conf->size]);
}

WorldState init_WorldState(WorldState w, WorldConf conf, MapState map) {
	*w = (S_WorldState) {
		conf: conf,
		free: conf->size,
		busy: 0,
		aliv: 0,
		tkns: 0,
		tick: 0,
		box: (Message) (w + 1),
		stk: (AgentState*) (((Message) (w + 1)) + conf->size),
		que: ((AgentState*) (((Message) (w + 1)) + conf->size)) + conf->size,
		pop: (AgentState) (((AgentState*) (((Message) (w + 1)) + conf->size)) + 2 * conf->size),
		map: map,
	};
	return w;
}

/****************************************/
// Life Cycle
/****************************************/
void World_wipe(WorldState w, Cell c, AgentState g) {
	if ( g->rslt != ResultTag_DEAD && g->rslt != ResultTag_ERRR && g->fuel )
		return;
	g->scor.liftm += w->tick;
	g->rslt = ResultTag_DEAD;
	Cell_kilAgent(c, g);
	w->aliv--;
}


AgentState World_addAgents(WorldState w, Nat32 agents, void* data) {

	if (w->free < agents) {
		printf("No more room to add %u agent(s) !\n", agents);
		exit(1);
	}

	for(Nat32 idx = w->busy, max = idx + agents; idx < max; idx++) {
		w->stk[idx] = w->pop + idx;
		w->pop[idx] = (S_AgentState) {
			id: idx,
			pos: {x: 0, y: 0},
			scor: {slctn: 0, score: 0, liftm: 0, ltpos: {0, 0}, gtpos: {0, 0}},
			tokn: w->conf->AGENT_TOKN_START,
			fuel: w->conf->AGENT_FUEL_START,
			gold: w->conf->AGENT_GOLD_START,
			prev: NULL,
			next: NULL,
			data: data,
			actn: ACTION(WAIT),
			rslt: RESULT(NULL),
		};
		Cell_addAgent(Map_respawn(w->map, &w->pop[idx]), &w->pop[idx]);
	}

	w->free -= agents;
	w->busy += agents;
	w->aliv += agents;
	return w->pop + w->busy - agents;
}



void World_step(WorldState w) {

	Nat64 scor = w->tick % 10 == 9 ? 1 : 0;

	//*********************************************************
	// For each cell, update its state and its agents'
	//*********************************************************
	for(Cell c = w->map->hots, c0 = c ? c->nxt : NULL; c; c = c0, c0 = c ? c->nxt : NULL) {
		//~ printf("cell update \n");
		AgentState actions[c->pop];
		AgentState *kaction = actions;

		//*********************************************************
		// First organise (randomize) the actions
		//*********************************************************
		for(AgentState g = c->ags, g0 = g->next; g; g = g0, g0 = g ? g->next : NULL)
			if (g->rslt == ResultTag_NULL) {

				//~ printf("agent update \n");

				g->scor.score += scor;

				Nat64 fcost = w->conf->COSTS[g->actn.tag].fuel;
				Nat64 gcost = w->conf->COSTS[g->actn.tag].gold;

				if ( fcost > g->fuel || gcost > g->gold ) {

					g->fuel -= MIN(g->fuel, fcost);
					g->rslt = ResultTag_FAIL;
					World_wipe(w, c, g);

				} else {

					g->fuel -= fcost;
					g->gold -= gcost;
					Nat64 rnd = RNGMOD(Nat64, kaction - actions + 1);
					kaction[0] = actions[rnd];
					actions[rnd] = g;
					kaction++;

				}
			}

		w->msgs = 0;

		//~ printf("process \n");
		//*********************************************************
		// Process the actions
		//*********************************************************
		for(AgentState *g0 = actions, g = *g0; g0 < kaction; g = *(++g0)) {
			Cell d = Action_process(w, c, g);
			Map_stepEffect(w->map, d, g);
			if (g->pos.x < g->scor.ltpos.x) g->scor.ltpos.x = g->pos.x;
			if (g->pos.y < g->scor.ltpos.y) g->scor.ltpos.y = g->pos.y;
			if (g->pos.x > g->scor.gtpos.x) g->scor.gtpos.x = g->pos.x;
			if (g->pos.y > g->scor.gtpos.y) g->scor.gtpos.y = g->pos.y;
			if (g->actn.tag != ActionTag_WAIT && g->rslt == RESULT(DONE)) g->scor.score++;
			World_wipe(w, d, g);
		}

	}

}


Nat64 writeSize_WorldMap(WorldState w) {
	return writeSize_MapState(w->map) + 100;
}

Nat64 write_WorldMap(char* buf, WorldState w) {
	Nat64 n = sprintf(buf, "epoch: %6u,\t\ttokens; %4u,\t\talive: %5u,\t\tdead: %5u\n", w->tick, w->tkns, w->aliv, w->conf->size - w->free - w->aliv);
	return n + write_MapState(buf + n, w->map);
}













void World_run(WorldState w, void (Actioner)(WorldState), RunConf conf) {

	if (conf->ANIMATE || conf->SHOW_START) {
		PRINT(WorldMap, w);
		if (conf->ANIMATE && conf->WAIT) { char buf[1]; fread(buf, 1, 1, stdin); }
	}

	for(Nat32 q = 0; q < w->busy; q++) {
		w->pop[q].scor.ltpos = w->pop[q].pos;
		w->pop[q].scor.gtpos = w->pop[q].pos;
	}

	for(Nat32 epchs = w->conf->epchs; w->tick < epchs && w->aliv && (!w->conf->GAME_TOKN_TRGT || w->tkns < w->conf->GAME_TOKN_TRGT); w->tick++) {

		Actioner(w);
		World_step(w);
		//~ Map_step(w->map, w->tick);

		if (conf->ANIMATE && w->tick % conf->ANIMATE == 0) {
			PRINT(WorldMap, w);
			usleep(conf->TIMEOUT);
		}

	}

	if (conf->ANIMATE || conf->SHOW_END)
		PRINT(WorldMap, w);
}



void World_reset(WorldState w) {

	AgentState a;
	for(Nat32 q = 0; q < w->busy; q++) {
		a = &w->pop[q];
		a->prev = NULL;
		a->next = NULL;
		a->tokn = w->conf->AGENT_TOKN_START;
		a->fuel = w->conf->AGENT_FUEL_START;
		a->gold = w->conf->AGENT_GOLD_START;
		a->actn = ACTION(WAIT);
		a->rslt = RESULT(NULL);
		Cell_addAgent(Map_respawn(w->map, a), a);
		w->pop[q].scor.ltpos = w->pop[q].pos;
		w->pop[q].scor.gtpos = w->pop[q].pos;
	}
	w->aliv = w->busy;
	w->tkns = 0;
	w->tick = 0;
	w->msgs = 0;
}





