#if SECTION == HEADERS

	#define ACTION(T, ...)		(CONSTRUCTOR(Action, T, __VA_ARGS__))
	#define RESULT(T)		(ResultTag_##T)

	typedef		Nat32				AId;
	typedef		struct Action*			Action;
	typedef		struct Message*			Message;
	typedef		struct Message			S_Message;
	typedef		struct AgentState*		AgentState;
	typedef		struct RunConf*			RunConf;
	typedef		struct WorldConf*		WorldConf;
	typedef		struct WorldState*		WorldState;
	typedef		struct Action			(*Runner)(WorldState, Cell, AgentState);

#endif

#define CONTEXT Result
	TAGS(Unit, (
		TAG(NULL, ({}))
		TAG(ERRR, ({}))
		TAG(DEAD, ({}))
		TAG(FAIL, ({}))
		TAG(MISS, ({}))
		TAG(DONE, ({}))
		TAG(SUCC, ({}))
	))
#undef CONTEXT

#define CONTEXT Msg
	TAGS(Unit, (
		TAG(FUEL, ({}))
		TAG(GOLD, ({}))
		TAG(GOAL, ({}))
		TAG(TOKN, ({}))
		TAG(PICK, ({}))
		TAG(HOLD, ({}))
		TAG(DROP, ({}))
		TAG(TRAP, ({}))
		TAG(HELP, ({}))
	))
#undef CONTEXT

#define CONTEXT Action

	#define METHODS(T, proc)	Cell Action_process##T(WorldState w, Cell c, AgentState g, S_ActionData_##T data) proc
	#define RETURN(R, C)		{ g->rslt = RESULT(R); return C; }

	TYPE(Unit, (), (
		CLASS(WAIT, ({}), Unit, ({ RETURN(DONE, c) }))
		CLASS(MARK, ({}), Boo,  ({ c->mrk = data; RETURN(DONE, c); }))
		CLASS(MOVE, ({}), DirTag, ({

			if ( data > 4 )
				throw("Non authorized move %s", Dir_Names[data]);

			Cell d = Cell_delta(c, Dir_Data[data]);
			if ( d->tag == CellTag_VOID )				RETURN(DEAD, c);
			if ( d->tag == CellTag_WALL )				RETURN(FAIL, c);
			if ( c->tag == CellTag_TRAP && c->data.TRAP )		RETURN(FAIL, c);
			if ( c->tag == CellTag_SINK && data == DirTag_HERE ) {
				d = Map_respawn(w->map, g);
				Cell_remAgent(c, g);
				Cell_addAgent(d, g);
				RETURN(DONE, d);
			}
			Cell_remAgent(c, g); Cell_addAgent(d, g);		RETURN(DONE, d);

		}))
		CLASS(FUEL, ({}), Nat64, ({
			Nat64 qtt = MIN(MIN(w->conf->AGENT_FUEL_MAX - (g->fuel + w->conf->COSTS[ActionTag_FUEL].fuel), data), c->fuel);
			c->fuel -= qtt;
			g->fuel += qtt + (qtt ? 1 : 0);
			if (qtt) RETURN(DONE, c);
			RETURN(MISS, c);
		}))
		//~ CLASS(ATTK, ({}), DirTag, ({

		//~ })
	))

	#undef RETURN
	#undef METHODS

#undef CONTEXT

#if SECTION == STRUCTS

	typedef struct RunConf {
		Boo			WAIT;
		Nat64			ANIMATE;
		Nat64			TIMEOUT;
		Boo			SHOW_START;
		Boo			SHOW_END;
	} S_RunConf;

	typedef struct Message {
		MsgTag		tag;
		Pos32		pos;
		Pos32		dlt;
	} S_Message;

	typedef struct Score {
		Nat64		slctn;
		Nat64		liftm;
		Nat64		score;
		Pos32		ltpos;
		Pos32		gtpos;
	} Score;

	typedef struct AgentState {
		Nat32		id;
		Pos32		pos;
		Score		scor;
		Boo		tokn;
		Nat64		fuel;
		Nat64		gold;
		AgentState	prev;
		AgentState	next;
		void*		data;
		S_Action	actn;
		ResultTag	rslt;
		Pos32		dlta;
	} S_AgentState;

	typedef struct {
		Nat64		fuel;
		Nat64		gold;
	} Costs;

	typedef struct WorldConf {
		Nat32		size;
		Nat32		epchs;
		Nat32		GAME_TOKN_TRGT;
		Boo		AGENT_TOKN_START;
		Nat64		AGENT_FUEL_MAX;
		Nat64		AGENT_FUEL_START;
		Nat64		AGENT_GOLD_MAX;
		Nat64		AGENT_GOLD_START;
		Costs		COSTS[_ActionTags_];
	} S_WorldConf;

	typedef struct WorldState {
		WorldConf	conf;
		Nat32		free;
		Nat32		busy;
		Nat32		aliv;
		Nat32		tkns;
		Nat32		tick;
		Nat32		msgs;
		Message		box;
		AgentState*	stk;
		AgentState*	que;
		AgentState	pop;
		MapState	map;
	} S_WorldState;

#endif

#if SECTION == PRECODE
	#define Actions_count 5
	S_Action Actions_Dad[Actions_count] = {
		ACTION(WAIT),
		ACTION(MOVE, DirTag_NORT),
		ACTION(MOVE, DirTag_SOUT),
		ACTION(MOVE, DirTag_WEST),
		ACTION(MOVE, DirTag_EAST),
	};

#endif




	//~ ACTION(TURN, DirTag_)



			//~ if (d->pop > 5) RETURN(FAIL, c);
				//~ return c->tag == CellTag_FUEL;
			//~ if ( d->tag == CellTag_ROCK )			RETURN(FAIL, c);
			//~ if ( d->tag == CellTag_ROOM && !d->data.room.open )	RETURN(FAIL, c);
			//~ if ( c->tag == CellTag_ROOM && !c->data.room.open )	RETURN(FAIL, c);
			//~ Boo isRespawn(Cell c) {
				//~ return c->tag == CellTag_CELL && c->pop < 10;
			//~ }

		//~ CLASS(SMSH, ({}), DirTag, ({
			//~ Cell d = Cell_delta(c, Dir_Data[data]);
			//~ if ( d->tag )
			//~ RETURN(DONE, c);
		//~ }))
	//~ ActionTag_MARK,
	//~ ActionTag_BCST,
	//~ ActionTag_FUEL,
	//~ ActionTag_DIGG,
	//~ ActionTag_FLAG,
	//~ ActionTag_HELP,
	//~ ActionTag_SMSH,
	//~ ActionTag_DROP,
	//~ ActionTag_PICK,
	//~ ActionTag_OPEN,
	//~ ActionTag_CLOZ,



