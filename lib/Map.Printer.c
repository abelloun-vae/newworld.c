#include "./Map.Printer.utf8.c"

#define FONT_GREEN0   "\e[38;2;0;255;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_GREEN1   "\e[38;2;0;204;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_GREEN2   "\e[38;2;0;153;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_GREEN3   "\e[38;2;0;102;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_GREEN4   "\e[38;2;0;51;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_RED     "\e[38;2;200;0;0m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define FONT_BLUE    "\e[38;2;0;0;200m" // where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BCKGRD_RED   "\e[48;2;200;0;0m" // where rrr;ggg;bbb in 48;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BCKGRD_GREEN "\e[48;2;0;200;0m" // where rrr;ggg;bbb in 48;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BCKGRD_BLUE  "\e[48;2;0;0;200m" // where rrr;ggg;bbb in 48;2;rrr;ggg;bbbm can go from 0 to 255 respectively

#define STYLE_RESET     "\x1b[0m"

#define BG_VOID		"\e[48;2;55;0;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_TRAP		"\e[48;2;150;0;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_STRT		"\e[48;2;0;0;185m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_GOAL		"\e[48;2;0;0;255m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_PLTF		"\e[48;2;150;150;150m\e[38;2;0;0;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_FLAG		"\e[48;2;255;0;255m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_HOVR		"\e[48;2;150;0;150m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_QBIT		"\e[48;2;255;255;0m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_CLCK		"\e[48;2;150;150;0m\e[38;2;255;255;255m"	// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_ORCL		"\e[48;2;100;100;0m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively

#define BG_ROOM_0	"\e[48;2;0;100;100m\e[38;2;255;255;255m"	// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_ROOM_1	"\e[48;2;0;255;255m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively

#define BG_FUEL_0	"\e[48;2;0;0;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_FUEL_1	"\e[48;2;0;102;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_FUEL_2	"\e[48;2;0;153;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_FUEL_3	"\e[48;2;0;204;0m"				// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define BG_FUEL_4	"\e[48;2;0;255;0m\e[38;2;0;0;0m"		// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively

#define CHR_TOKEN_0	"\e[38;2;255;0;0m*\x1b[0m"			// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively
#define CHR_TOKEN_1	"\e[38;2;255;255;0m*\x1b[0m"			// where rrr;ggg;bbb in 38;2;rrr;ggg;bbbm can go from 0 to 255 respectively


Nat64 writeSize_MapSolution(MapState map) {
	return 20 * MAPSIZE(map);
}

Nat64 writeSize_MapStateView(MapState map) {
	return 20 * MAPSIZE(map);
}

Nat64 writeSize_MapState(MapState map) {
	return 20 * MAPSIZE(map);
}

Nat64 write_MapSolution_(char* buf, MapState map, Policy* solution, Int32, Int32);

Nat64 write_MapStateView(char* buf, MapState map, Int32 x, Int32 y) {
	return write_MapSolution_(buf, map, NULL, x, y);
}

Nat64 write_MapSolution(char* buf, MapState map, Policy* solution) {
	return write_MapSolution_(buf, map, solution, 0, 0);
}

Nat64 write_MapState(char* buf, MapState map) {
	return write_MapSolution(buf, map, NULL);
}

Nat64 write_MapSolution_(char* out, MapState map, Policy* solution, Int32 xx, Int32 yy) {

	Cell c;
	MATRIX(map, m);
	char* cur = out;
	Nat32 w = map->conf->dim.w;
	Nat32 h = map->conf->dim.h;

	Boo tokenInAgents(AgentState as) {
		return false;
		while (as) {
			if (as->tokn) return true;
			as = as ->next;
		}
		return false;
	}

	char* getFuelLevel(Cell c, Nat64 f) {
		//~ printf("rate: %lu, incr: %lu \n", c->data.FUEL.rate, c->data.FUEL.incr);
		char* levels[] = {
			BG_FUEL_0,
			BG_FUEL_1,
			BG_FUEL_2,
			BG_FUEL_3,
			BG_FUEL_4,
			BG_FUEL_4,
		};
		return levels[f * 5 / map->conf->CELL_FUEL_MAX];
	}

	char* getFuelLevel2(Cell c, Nat64 f) {
		char* levels[] = {
			BG_FUEL_0,
			BG_FUEL_1,
			BG_FUEL_2,
			BG_FUEL_3,
			BG_FUEL_4,
			BG_FUEL_4,
		};
		return levels[MIN(f * 5 / (500), 4)];
	}

	Nat64 write_Wall(Cell c) {
		MATRIX(map, m);
		Nat32 row = c->pos.y;
		Nat32 col = c->pos.x;
		Nat64 xxx = c - map->cells;
		Nat32 yy = xxx / map->conf->dim.w;
		Nat32 xx = xxx % map->conf->dim.w;
		if (xx != col || yy != row)
			throw("Problemo !");
		//~ return sprintf(cur, "%c", '#');
		return sprintf(cur, "%s", syms[
			(row != 0 && m[row-1][col].tag == CellTag_WALL ? 1 : 0) +
			(row != map->conf->dim.h - 1 && m[row+1][col].tag == CellTag_WALL ? 1 : 0) * 2 +
			(col != 0 && m[row][col-1].tag == CellTag_WALL ? 1 : 0) * 4 +
			(col != map->conf->dim.w - 1 && m[row][col+1].tag == CellTag_WALL ? 1 : 0) * 8
		]);
	}

	Nat64 write_Pop(Cell c, char* defChar, char* style) {
		char syms[5];
		syms[DirTag_HERE] = '=';
		syms[DirTag_NORT] = '^';
		syms[DirTag_SOUT] = 'v';
		syms[DirTag_WEST] = '<';
		syms[DirTag_EAST] = '>';

		if ( solution && c->tag != CellTag_VOID )
			return sprintf(cur, "%s%c%s", style ?: "", syms[solution[c - map->cells].dir], style ? STYLE_RESET : "");

		char x[2];
		x[1] = 0;
		char* y = x;
		x[0] = 's';
		if (c->tkns)
			y = CHR_TOKEN_0;

		else if (tokenInAgents(c->ags))
			y = CHR_TOKEN_1;

		else if (!c->pop)
			y = defChar;

		else if (c->pop == 1)
			x[0] = 'a' + c->ags->id % 26;

		else if (c->pop < 28)
			x[0] = 'A' + c->pop - 2;

		else if (c->pop >= 28)
			x[0] = '0' + MIN(c->pop / 28 - 1, 9);

		return sprintf(cur, "%s%s%s", style ?: "", y, style ? STYLE_RESET : "");

	}

	for(Int32 y = 0; y < (Int32) h; y++) {

		for(Int32 x = 0; x < (Int32) w; x++) {

			if (xx && yy && xx == x && yy == y) {
				*(cur++) = 'X';
				continue;
			}

			c = &m[y][x];
			switch(c->tag) {
				case CellTag_WALL: cur += write_Wall(c); continue;
				case CellTag_VOID: cur += write_Pop(c, " ", BG_VOID); continue;
				case CellTag_CELL: cur += write_Pop(c, c->mrk ? " " : cellMrkd, getFuelLevel2(c, c->fuel)); continue;
				case CellTag_STRT: cur += write_Pop(c, "%", BG_STRT); continue;
				case CellTag_GOAL: cur += write_Pop(c, omega, BG_GOAL); continue;
				case CellTag_SINK: cur += write_Pop(c, "@", BG_GOAL); continue;
				case CellTag_TRAP: cur += write_Pop(c, c->data.TRAP ? "!" : " ", BG_TRAP); continue;
				case CellTag_FUEL: cur += write_Pop(c, fuel, getFuelLevel(c, c->fuel)); continue;
				case CellTag_GOLD: cur += write_Pop(c, "$", getFuelLevel(c, c->gold)); continue;
				case CellTag_HOVR: cur += write_Pop(c, " ", BG_HOVR); continue;
				case CellTag_FLAG: cur += write_Pop(c, c->data.FLAG ? "+" : "-", BG_FLAG); continue;
				//~ case CellTag_INFO: cur += write_Pop(c, "?", BG_STRT); continue;
				case CellTag_QBIT: cur += write_Pop(c, c->data.QBIT ? "+" : "-", BG_QBIT); continue;
				case CellTag_CLCK: cur += write_Pop(c, c->data.CLCK.on ? "+" : "-", BG_CLCK); continue;
				case CellTag_ORCL: cur += write_Pop(c, c->data.ORCL.on ? "+" : "-", BG_ORCL); continue;
				//~ case CellTag_ROOM: cur += write_Pop(c, " ", c->data.room.open ? BG_ROOM_0 : BG_ROOM_1); continue;
				//~ case CellTag_PLTF: cur += write_Pop(c, "_", BG_PLTF); continue;
				case CellTag_UNKW: cur += write_Pop(c, "?", NULL); continue;
				case _CellTags_: throw("Unknow cell type : %u", c->tag);

			}

			*(cur++) = 's';
			continue;
		}

		*(cur++) = '\n';

	}

	return cur - out;
}
