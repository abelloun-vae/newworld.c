/**************************************************************/
//
/**************************************************************/
#define		ID(X)					X
#define		APPLY(F, A)				F A
#define		EXPLODE(...)				__VA_ARGS__
#define		EXPLODE2(...)				__VA_OPT__(, __VA_ARGS__)
#define		CONCAT(C, D)				C##D
#define		CONCAT3(C, D, E)			C##D##E


#define		OFFSET(T, M, ptr)			((T)(((char*)(ptr)) - offsetof(S_##T, M)))
#define		ACCESS(T, O, ptr)			(*((T*)(((char*)(ptr)) + (O))))


#define		ALLOC(size)				myalloc(size)
#define		ALLOCS(...) ({\
			Nat64 _sizes[] = {__VA_ARGS__};\
			Nat64 _count = sizeof(_sizes) / sizeof(_sizes[0]);\
			Nat64 _size = sizeof(void*[_count]); for(Nat64 _i = 0; _i < _count; _i++) _size += _sizes[_i];\
			void* _mem = ALLOC(_size);\
			for(Nat64 _i = 0, _cur = sizeof(void*[_count]), *arr = _mem; _i < _count; _cur += _sizes[_i], _i++)\
				((void**)_mem)[_i] = ((char*)_mem) + _cur;\
			_mem;\
		})



#define		INIT(T, ptr, ...)			(init_##T(ptr __VA_OPT__(, __VA_ARGS__)))


#define		CONSTRUCTOR(T, t, ...) ((S_##T) {\
			tag:T##Tag_##t __VA_OPT__(,\
			data: {t: __VA_ARGS__ }	/*//~ data: {t: (S_##T##Data_##t) __VA_ARGS__ }\*/ \
		)})



#define		NEW(T, sParams, ...) ({\
			Nat64 _size = sizeof_##T sParams;\
			if (0) printf("Allocate %lu bytes for NEW(" #T ", " #sParams ", " #__VA_ARGS__ ").\n", _size);\
			INIT(T, ALLOC(_size) EXPLODE2 sParams __VA_OPT__(, __VA_ARGS__));\
		})

#define		PRINT(T, x, ...) {\
			typeof(x) _x = (x);\
			Nat64 _size = writeSize_##T(_x);\
			char* _buf = ALLOC(_size);\
			Nat64 _z = write_##T(_buf, _x __VA_OPT__(, __VA_ARGS__));\
			if (0) printf("Allocate %lu bytes (%lu written) for PRINT(" #T ", " #x ", " #__VA_ARGS__ ").\n", _size, _z);\
			if (1) fwrite(_buf, 1, _z, stdout);\
			free(_buf);\
		}


/**************************************************************/
//
/**************************************************************/
#define		HEADERS	1
#define		PRELUDE	6
#define		DATAS	2
#define		UNIONS	3
#define		STRUCTS	4
#define		CLASSDT 7
#define		PRECODE	5

//~ #define TAG(Name, DataValue)
//~ #define TAGS(Data, Tags)
//~ #define CLASS(Name, Type)
//~ #define TYPE(Props, Classes)
/**************************************************************/
// Headers generation
/**************************************************************/
#define		SECTION	HEADERS
#define		TAG(D, Dat)				APPLY(CONCAT, (CONTEXT, Tag_##D)),
#define		TAGS(DatTyp, Tags)			typedef enum APPLY(CONCAT, (CONTEXT, Tag)) {EXPLODE Tags APPLY(CONCAT3, (_, CONTEXT, Tags_))} APPLY(CONCAT, (CONTEXT, Tag));
#define		CLASS(D, Dat, P, ...)			APPLY(CONCAT, (CONTEXT, Tag_##D)),
#define		TYPE(DatTyp, Props, Impls) \
		typedef		struct CONTEXT*		CONTEXT;\
		typedef		struct CONTEXT		APPLY(CONCAT, (S_, CONTEXT));\
		typedef		enum APPLY(CONCAT, (CONTEXT, Tag)) \
		{EXPLODE Impls	APPLY(CONCAT3, (_, CONTEXT, Tags_))}\
		APPLY(CONCAT, (CONTEXT, Tag));

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION


/**************************************************************/
// PRELUDE (Enum Set)
/**************************************************************/
#define		SECTION	PRELUDE
#define		TAG(D, Dat)				//APPLY(CONCAT, (CONTEXT, Tag_##D)),
#define		TAGS(DatTyp, Tags)			//typedef enum {EXPLODE Tags} APPLY(CONCAT, (CONTEXT, Tag));
#define		CLASS(D, Dat, P, ...)			APPLY(CONCAT, (CONTEXT, Tags_##D)) = 1 << APPLY(CONCAT, (CONTEXT, Tag_##D)),
#define		TYPE(DatTyp, Props, Impls) \
		typedef		enum APPLY(CONCAT, (CONTEXT, Tags)) \
		{EXPLODE Impls}	APPLY(CONCAT, (CONTEXT, Tags));


#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION


/**************************************************************/
// Data struct generation
/**************************************************************/
#define		SECTION DATAS
#define		TAG(D, Dat)
#define		TAGS(DatTyp, Tags)
#define		CLASS(D, Dat, P, ...)			typedef P APPLY(CONCAT, (APPLY(CONCAT, (S_, CONTEXT)), Data_##D));
#define		TYPE(DatTyp, Props, Impls)		EXPLODE Impls

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION


/**************************************************************/
// Data union generation
/**************************************************************/
#define		SECTION UNIONS
#define		TAG(D, Dat)				EXPLODE Dat,
#define		TAGS(DatTyp, Tags)			DatTyp APPLY(CONCAT, (CONTEXT, _Data))[] = (DatTyp[]) { EXPLODE Tags };
#define		CLASS(D, Dat, P, ...)			APPLY(CONCAT, (APPLY(CONCAT, (S_, CONTEXT)), Data_##D)) D;
#define		TYPE(DatTyp, Props, Impls)		typedef union { EXPLODE Impls } APPLY(CONCAT, (APPLY(CONCAT, (S_, CONTEXT)), Data));

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION


/**************************************************************/
// Class struct generation and names
/**************************************************************/
#define		SECTION STRUCTS
#define		TAG(D, Dat)			#D,
#define		TAGS(DatTyp, Tags)		char* APPLY(CONCAT, (CONTEXT, _Names))[] = { EXPLODE Tags TAG(_< CONTEXT Tag>_, DUMMY) };
#define		CLASS(D, Dat, P, ...)		#D,
#define		TYPE(DatTyp, Props, Impls)	struct CONTEXT {\
			APPLY(CONCAT, (CONTEXT, Tag))	tag;\
			EXPLODE Props\
			APPLY(CONCAT, (APPLY(CONCAT, (S_, CONTEXT)), Data)) data;\
		};\
		char* APPLY(CONCAT, (CONTEXT, _Names))[] = { EXPLODE Impls TAG(_< CONTEXT Tag>_, DUMMY) };

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION

/**************************************************************/
//
/**************************************************************/
#define		SECTION CLASSDT
#define		TAG(D, Dat)
#define		TAGS(DatTyp, Tags)
#define		CLASS(D, Dat, P, ...)		EXPLODE Dat,
#define		TYPE(DatTyp, Props, Impls)	DatTyp APPLY(CONCAT, (CONTEXT, _Data))[] = (DatTyp[]) { EXPLODE Impls };

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION

/**************************************************************/
//
/**************************************************************/
#define		SECTION PRECODE
#define		TAG(D, Dat)
#define		TAGS(DatTyp, Tags)
#define		CLASS(D, Dat, P, ...)		__VA_OPT__(METHODS(D, EXPLODE __VA_ARGS__))
#define		TYPE(DatTyp, Props, Impls)	EXPLODE Impls

#include _TARGET_

#undef		TAG
#undef		TAGS
#undef		CLASS
#undef		TYPE
#undef		SECTION


/**************************************************************/
//
/**************************************************************/
#undef		HEADERS
#undef		PRELUDE
#undef		DATAS
#undef		UNIONS
#undef		STRUCTS
#undef		CLASSDT
#undef		PRECODE

