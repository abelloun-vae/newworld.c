from keras.models import Sequential, model_from_json
from keras.layers import Dense, LSTM
import json, numpy as np



class Colony:

	def __init__(this, inputSize, outputLayer, hiddenLayers):
		this.inputSize = inputSize
		this.outputLayer = outputLayer
		


class Agent:
	
	
	@staticmethod
	def initFromSpec(InputSize, OutputLayer, HiddenLayers):

		# #################
		def make_neuron(params):
			if params["activation"] == "linear":
				cons = tf.keras.constraints.min_max_norm(min_value=params["min"], max_value=params["max"])
				return tf.keras.layers.Dense(units=params["units"], activation='linear', kernel_constraint=cons)

			elif params["activation"] == "softmax":
				return tf.keras.layers.Dense(units=params["units"], activation='softmax')

			return tf.keras.layers.Dense(units=params["units"], activation=params["activation"])
		
		# #################
		inputLayer = tf.keras.layers.Input(shape=(InputSize,))
		
		previousLayer = inputLayer
		for layer in HiddenLayers:
		    previousLayer = tf.keras.layers.concatenate([make_neuron(neuron)(previousLayer) for neuron in layer], axis=-1)

		outputLayer = tf.keras.layers.concatenate([make_neuron(neuron)(previousLayer) for neuron in OutputLayer], axis=-1)
		
		return Agent(tf.keras.models.Model(inputs=inputLayer, outputs=outputLayer))




	@staticmethod
	def makeFromJsonFile(filename):
		with open(filename, 'r') as f:
			model_config = json.load(f)
		return Agent(model_from_json(model_config))




	# #################
	def __init__(this, model):
		this.model = model




	# #################
	def randomize(this):
		for layer in this.model.layers:
			current_weights = layer.get_weights()
			new_weights = []
			for weight in current_weights:
				random_weight = np.random.normal(loc=0.0, scale=0.1, size=weight.shape)
				new_weights.append(random_weight)
			layer.set_weights(new_weights)




	# #################
	def dumpToJsonFile(this, filename):
		model_config = this.model.to_json()
		with open(filename, 'w') as f:
			json.dump(model_config, f)


		    
		    
		    